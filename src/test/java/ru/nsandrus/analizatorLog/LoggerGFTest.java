package ru.nsandrus.analizatorLog;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

public class LoggerGFTest  {

	@Test
	public void buildMessage1() throws IOException {
		LoggerGF logger = new LoggerGF("server1.log");
		String message = logger.getMessage();
		Assert.assertEquals("[#|20 первое сообщение ssageEndpoint|#]",message);
		
	}
	
	@Test
	public void buildMessageNotBegin() throws IOException {
		LoggerGF logger = new LoggerGF("server2.log");
		String message = logger.getMessage();
		Assert.assertEquals("[#|2016-12-16T14:06:33.385+0300|SEVERE|sun-appserver2.1|sun-jms-binding.com.sun.jbi.jmsbc.InboundMessageProcessorFactory|_ThreadID=2449;_ThreadName=JMSJCA sync #1(TT02);Context=TroubleTicket-sun-jms-binding::SystemsQueue_PortTTSystems::SystemsOperation;_RequestID=628e4c71-558a-4d42-bde2-027ab9563815;|JMSBC-E0701: Failed to create a MessageEndpoint|#]",message);
		String message2 = logger.getMessage();
		Assert.assertEquals("[#|2016-12-16T14:06:33.383+0300|SEVERE|sun-appserver2.1|sun-jms-binding.com.sun.jbi.jmsbc.InboundMessageProcessorFactory|_ThreadID=2455;_ThreadName=JMSJCA sync #7(TT02);Context=TroubleTicket-sun-jms-binding::SystemsQueue_PortTTSystems::SystemsOperation;_RequestID=df9c68c0-eb45-4b8d-8e07-615394ebf461;|JMSBC-E0701: Failed to create a MessageEndpoint|#]",message2);
		
	}
	
	@Test
	public void buildMessageNotBeginMultiLine() throws IOException {
		LoggerGF logger = new LoggerGF("server3.log");
		String message = logger.getMessage();
		Assert.assertEquals("[#|2016-12-16T14:06:33.385+0300|SEVERE|sun-appserver2.1|sun-jms-binding.com.sun.jbi.jmsbc.InboundMessageProcessorFactory|_ThreadID=2449;\n_ThreadName=JMSJCA sync #1(TT02);Context=TroubleTicket-sun-jms-binding::SystemsQueue_PortTTSystems::SystemsOperation;_RequestID=628e4c71-558a-4d42-bde2-027ab9563815;|\nJMSBC-E0701: Failed to create a MessageEndpoint|#]",message);
		String message2 = logger.getMessage();
		Assert.assertEquals("[#|2016-12-16T14:06:33.383+0300|SEVERE|sun-appserver2.1|sun-jms-binding.com.sun.jbi.jmsbc.InboundMessageProcessorFactory|_ThreadID=2455;_ThreadName=JMSJCA sync #7(TT02);Context=TroubleTicket-sun-jms-binding::SystemsQueue_PortTTSystems::SystemsOperation;_RequestID=df9c68c0-eb45-4b8d-8e07-615394ebf461;|JMSBC-E0701: Failed to create a MessageEndpoint|#]",message2);
		
	}
	
	@Test
	public void notFukkMessage() throws IOException {
		LoggerGF logger = new LoggerGF("server4.log");
		String message = logger.getMessage();
		Assert.assertEquals("",message);
		
	}

}
