package ru.nsandrus.avs;


import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import ru.nsandrus.enviroment.Config;
import ru.nsandrus.file.FileReader;
import ru.nsandrus.file.ReadFileException;

/**
 * Unit test for simple App.
 */
public class PlanAvsTest 
    extends TestCase
{

	
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public PlanAvsTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( PlanAvsTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
    
    
  

    public void testApp3() throws ReadFileException
    {
    	String text = FileReader.readFile("testRead.file");
        assertEquals("test read", text);
    }

    
 
}
