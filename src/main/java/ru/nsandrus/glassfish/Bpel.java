package ru.nsandrus.glassfish;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByName;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import com.fasterxml.jackson.databind.ser.std.CalendarSerializer;
import com.ibm.msg.client.commonservices.Log.Log;

import ru.nsandrus.avs.Scenario;
import ru.nsandrus.entity.DomainGf;
import ru.nsandrus.exeptions.ScenarioException;

import static com.codeborne.selenide.Selenide.*;

import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ListIterator;
import java.util.concurrent.TimeUnit;

public class Bpel {

	DomainGf domain;
	Logger LOG = Logger.getLogger(Bpel.class);

	public Bpel(DomainGf domain2) {
		domain = domain2;
	}

	public String testLogin() throws ScenarioException {

		System.out.println("Start");
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss z");
		System.out.println("date: " + dateFormat.format(new Date()));

		WebDriver driver = new HtmlUnitDriver(true);
		// System.setProperty("selenide.timeout", "5000");
		driver.manage().timeouts().implicitlyWait(domain.implicitlyWai, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(domain.pageLoadTimeout, TimeUnit.SECONDS);
		WebDriverRunner.setWebDriver(driver);

		open(domain.url + "/login.jsf");
		$(By.name("j_username")).setValue(domain.login);
		$(By.name("j_password")).setValue(domain.password);
		// System.out.println("login=["+domain.login+"]
		// pass=["+domain.password+"]");
		$("#loginButton").click();

		if ($("#sun_label34").isDisplayed()) {
			throw new ScenarioException("Authentication Failed! Re-enter your username and password");
		}

		// String domain = domain.url; //"http://ms-glass006:7048";
		open(domain.url + "/jbi/pe/bindingsEngines.jsf?filterType=Show%20All");
		System.out.println("2 date: " + dateFormat.format(new Date()));

		open(domain.url + "/jbi/pe/showBindingOrEngine.jsf?type=service-engine&name=sun-bpel-engine");

		System.out.println("3 date: " + dateFormat.format(new Date()));
		$(By.id("tabsForm:jbiShowTabs:application")).click();
		open(domain.url + "/jbi/pe/showCompAppConfigs.jsf?name=sun-bpel-engine&type=service-engine&tname=server#");
		System.out.println("4 date: " + dateFormat.format(new Date()));

		$(By.id("tabsForm:jbiShowTabs:application:showCompAppTabs:variables")).click();
		System.out.println("vars date: " + dateFormat.format(new Date()));
		
		String result = printValueOfAll();
		System.out.println("result date: " + dateFormat.format(new Date()));
		return result;
		
	}

	private static void printValueSunHttpConfiguration(String urlName) {
		open("http://ms-glass006:7048/jbi/pe/showBindingOrEngine.jsf?type=binding-component&name=sun-http-binding");
		$(By.id("tabsForm:jbiShowTabs:application")).click();
		ElementsCollection tr = $$(By.tagName("tr"));
		open("http://ms-glass006:7048/jbi/pe/editAppConfig.jsf?compName=sun-http-binding&appConfigName=" + urlName);
		System.out.println(urlName + "=" + $(By.id("editAppConfigTableForm:compAppConfigPS:grp0Pss:prop1Prop:prop1TxtFld")).getValue());
	}

	private static void printValueSunHttpVariable(String urlName) {
		open("http://ms-glass006:7048/jbi/pe/showBindingOrEngine.jsf?type=binding-component&name=sun-http-binding");
		$(By.id("tabsForm:jbiShowTabs:application")).click();
		$(By.id("tabsForm:jbiShowTabs:application:showCompAppTabs:variables")).click();

		ElementsCollection array4 = $$(By.tagName("span"));
		String idName = array4.find(Condition.exactText(urlName)).attr("id");
		int indexLast = idName.lastIndexOf(':');
		idName = idName.substring(0, indexLast);
		indexLast = idName.lastIndexOf(':');
		idName = idName.substring(0, indexLast);
		System.out.println(urlName + "=" + $(By.id(idName + ":valueColumn:stringValueTextField")).getValue());
	}

	private static void setValueOf(String string, String value) {
		ElementsCollection array4 = $$(By.tagName("span"));
		String idName = array4.find(Condition.exactText(string)).attr("id");
		int indexLast = idName.lastIndexOf(':');
		idName = idName.substring(0, indexLast);
		indexLast = idName.lastIndexOf(':');
		idName = idName.substring(0, indexLast);
		$(By.id(idName + ":valueColumn:stringValueTextField")).setValue(value);
		$(By.id("showCompAppVarsTableForm:showCompAppVarsPageTitle:topButtons:saveButton")).click();
	}

	private static String printValueOf(String string) {
		ElementsCollection array4 = $$(By.tagName("span"));
		String idName = array4.find(Condition.exactText(string)).attr("id");
		int indexLast = idName.lastIndexOf(':');
		idName = idName.substring(0, indexLast);
		indexLast = idName.lastIndexOf(':');
		idName = idName.substring(0, indexLast);
		System.out.println(string + "=" + $(By.id(idName + ":valueColumn:stringValueTextField")).getValue());
		return $(By.id(idName + ":valueColumn:stringValueTextField")).getValue();
	}

	private String printValueOfAll() {
		LOG.info("Cycle for store all vars");
		StringBuilder vars = new StringBuilder();

		ElementsCollection arrayId = $$(By.xpath("//*[contains(@id,\"showCompAppVarsTableForm:showCompAppVarsTable:compAppVarTableRowGroupId:\")]"));
		String stringArray = arrayId.toString();

		for (int i = 0; i < 500; i++) {

			String pattern = "showCompAppVarsTableForm:showCompAppVarsTable:compAppVarTableRowGroupId:" + i + ":nameColumn:nameText";
			String name = "";
			if (stringArray.contains(pattern)) {
				String text = stringArray;
				int index = text.indexOf(pattern);
				if (index > 0) {
					text = text.substring(index + pattern.length() + 2);
					int end = text.indexOf("<");
					text = text.substring(0, end);
					name = text;
				}

			}

			pattern = "showCompAppVarsTableForm:showCompAppVarsTable:compAppVarTableRowGroupId:" + i + ":valueColumn:stringValueTextField";
			if (stringArray.contains(pattern)) {
				addToVarsResult("String", vars, stringArray, pattern, name);
			} 
			
			pattern = "showCompAppVarsTableForm:showCompAppVarsTable:compAppVarTableRowGroupId:" + i + ":valueColumn:numberValueTextField";
			if (stringArray.contains(pattern)) {
				addToVarsResult("Number", vars, stringArray, pattern, name);
			}

			pattern = "showCompAppVarsTableForm:showCompAppVarsTable:compAppVarTableRowGroupId:" + i + ":valueColumn:valueDropDown";
			if (stringArray.contains(pattern)) {
				addToVarsResult("Boolean", vars, stringArray, pattern, name);
//				String result = getVarsResult(stringArray, pattern);
//				LOG.debug(name + ":: "+ "Boolean" +" ::" + result);
//				vars.append(name + "=" + result + "\n");
			}


		}

		LOG.debug("Vars BPEL=" + vars.toString());
		return vars.toString();
		
	}

	private void addToVarsResult(String string, StringBuilder vars, String stringArray, String stringField, String name) {
		String text = stringArray;
		int index = text.indexOf(stringField);
		if (index > 0) {
			text = text.substring(index + stringField.length() + 2);
			int beginValue = text.indexOf("value=\"") + 7;
			int end = text.indexOf("\">");
			if (end > beginValue) {
				text = text.substring(beginValue, end);
				LOG.debug(name + ":: "+ string +" ::" + text);
				vars.append(name + "=" + text + "\n");
			}
		}

		
	}
	
	private String getVarsResult(String stringArray, String stringField) {
		String text = stringArray;
		int index = text.indexOf(stringField);
		if (index > 0) {
			text = text.substring(index + stringField.length() + 2);
			int beginValue = text.indexOf("value=\"") + 7;
			int end = text.indexOf("\">");
			if (end > beginValue) {
				text = text.substring(beginValue, end);
				return text;
			}
		}
		return "";

		
	}


}