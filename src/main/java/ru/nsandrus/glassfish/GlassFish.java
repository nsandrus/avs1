package ru.nsandrus.glassfish;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.impl.Log4JLogger;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.WebDriverRunner;

import ru.nsandrus.avs.Step;
import ru.nsandrus.entity.DomainGf;
import ru.nsandrus.exeptions.ScenarioException;
import ru.nsandrus.testing.dataobject.Server;

public class GlassFish {

	
	Logger LOG = Logger.getLogger(GlassFish.class);
	
	DomainGf domain;
	private boolean login = false;
	public GlassFish(DomainGf domain) {
		this.domain = domain;
	}


	public String getBpelEngine() throws ScenarioException {
		Bpel bpel = new Bpel(domain);
		LOG.info("Bpel go to login");
		String response = bpel.testLogin();
//		System.out.println("response=" + response);
//		login();
//		goToBpel();
//		printValueOf("AutoProlongationSpeedExternalSystemId");
		return response;
	}

	private void goToBpel() {
//		$(By.linkText("/jbi/pe/bindingsEngines.jsf?filterType=Show%20All")).click();
		open(domain.url + "/jbi/pe/bindingsEngines.jsf?filterType=Show%20All");
		open(domain.url + "/jbi/pe/showBindingOrEngine.jsf?type=service-engine&name=sun-bpel-engine");
		$(By.id("tabsForm:jbiShowTabs:application")).click();
		open(domain.url + "/jbi/pe/showCompAppConfigs.jsf?name=sun-bpel-engine&type=service-engine&tname=server#");
		$(By.id("tabsForm:jbiShowTabs:application:showCompAppTabs:variables")).click();
		LOG.trace(WebDriverRunner.source());
	}

	private void printValueOf(String string) {
		ElementsCollection array4 = $$(By.tagName("span"));
		LOG.debug("span=" + array4);
		String idName = array4.find(Condition.exactText(string)).attr("id");
		int indexLast = idName.lastIndexOf(':');
		idName =idName.substring(0, indexLast);
		indexLast = idName.lastIndexOf(':');
		idName = idName.substring(0, indexLast);
		System.out.println(string + "=" + $(By.id(idName + ":valueColumn:stringValueTextField")).getValue());
	}
	
	private void login() {
		if (login) return;
		
//      Configuration.timeout = 6000;
	  WebDriver driver = new HtmlUnitDriver(true);
	  System.setProperty("selenide.timeout", "120000");
	  driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	  driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
	  WebDriverRunner.setWebDriver(driver );
	  LOG.debug("domain=" + domain) ;
	  open(domain.url + "/login.jsf");
		$(By.name("j_username")).setValue(domain.login);
		$(By.name("j_password")).setValue(domain.password);

		$("#loginButton").click();
		System.out.println("login");
//		LOG.debug("login to console glassfish" + $(By.id("form:tree:JBIRoot:_bindingsEngines_0_15:_bindingsEngines_0_15_link")));
//		System.out.println($(By.id("form:tree:tree_link")).text());
		login = true;
		
	}

}
