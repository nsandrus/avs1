package ru.nsandrus.glassfish;

import ru.nsandrus.testing.dataobject.Server;

public interface GlassFishApi {

		
		public boolean restartComponent(String name, Server server);
		public boolean shutdownComponent(String name, Server server);
		public boolean startComponent(String name, Server server);
		
		public boolean setBpelEngineItem(String name, Server server);
		
		public boolean getBpelEngine(String name, Server server);
		public boolean getHttpConfigurations(String name, Server server);
		public boolean getHttpVariables(String name, Server server);
		
}
