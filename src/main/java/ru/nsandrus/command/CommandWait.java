package ru.nsandrus.command;

import static ru.nsandrus.entity.KeyWords.WAIT;

import org.apache.log4j.Logger;

public class CommandWait {

	static Logger LOG = Logger.getLogger(CommandWait.class);

	public static void wait(String codeLine) {
		int start = codeLine.toUpperCase().indexOf(WAIT);
		String time = codeLine.substring(start + 5);
		LOG.debug("WAIT seconds=[" + time + "]");
		int second = Integer.parseInt(time);
		sleep(second);
	}

	private static void sleep(int second) {
		LOG.info("Засыпаем секунд на " + second);
		try {
			Thread.sleep(second * 1000);
		} catch (InterruptedException e) {
		}
	}

}
