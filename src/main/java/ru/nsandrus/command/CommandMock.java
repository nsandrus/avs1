package ru.nsandrus.command;

import static ru.nsandrus.entity.KeyWords.*;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import ru.nsandrus.exeptions.CommandException;
import ru.nsandrus.exeptions.ScenarioException;
import ru.nsandrus.file.ReadFileException;
import ru.nsandrus.mock.Mock;
import ru.nsandrus.mock.MockRunner;

public class CommandMock {
	Logger LOG = Logger.getLogger(CommandMock.class);

	public void stop(List<Mock> mocks, String text, List<MockRunner> runnerMocks) throws CommandException {
		int start = text.toUpperCase().indexOf(STOP);
		String nameMock = text.substring(start + 5).trim();
		LOG.debug("Mock name to stop=" + nameMock);

		int i = 0;
		boolean isFoundMock = false;
		for (MockRunner mockRun : runnerMocks) {
			if (mockRun.getNameMock().equals(nameMock)) {
				LOG.info("Нашел заглушку для остановки!" + nameMock);
				isFoundMock = true;
				try {
					mockRun.завершить_Работу().join();
					runnerMocks.remove(i);
					sleep(2);
					return;
				} catch (InterruptedException e) {
					String textError = "Ошибка при попытке остановить заглушку по команде STOP";
					System.out.println(textError);
					LOG.error(textError + e.getMessage());
				}
			}
			i++;
		}
		if (!isFoundMock) {
			throw new CommandException("Not found Mock to stop. Name is::" + nameMock + "::");
		}
	}

	public void start(List<Mock> mocks, String text, List<MockRunner> runnerMocks, String path, List<Thread> threads) throws ReadFileException, CommandException {
		// получить из текста имя заглушки, потом ее остановить.
		int start = text.toUpperCase().indexOf(START);
		String nameMock = text.substring(start + 6).trim();
		LOG.debug("Mock name to start=[" + nameMock + "]");

		boolean isFoundMock = false;
		for (Mock mock : mocks) {
			// нельзя просто остановить, можно дать команду обработчику например потоков.
			if (mock.name.equals(nameMock)) {
				LOG.debug("Нашел заглушка для запуска по имени=" + mock.name);
				isFoundMock = true;
				mock.readResponseFromFile(path);
				LOG.info("Запуск заглушки на порту=" + mock.port);
				MockRunner обработчик_Заглушки = new MockRunner(mock);
				// по идее можно хранить сами потоки и потом делать join при завершении потоков

				Thread thread = new Thread(обработчик_Заглушки);
				thread.start();
				threads.add(thread);
				runnerMocks.add(обработчик_Заглушки);
				sleep(2);

				return;
			}
		}
		if (!isFoundMock) {
			throw new CommandException("Not found Mock to start. Name is::" + nameMock + "::");
		}

	}

	private void sleep(int i) {
		LOG.info("Засыпаем секунд на " + i);
		try {
			Thread.sleep(i * 1000);
		} catch (InterruptedException e) {
		}

	}

	public void newResponse(List<Mock> mocks, String text, String path) throws ReadFileException {
		if (mocks == null) {
			LOG.error("mocks is null");
			System.out.println("----- Mock is null. Может забыли в конфиге прописать?");
			return;
		}
		int start = text.toUpperCase().indexOf(NEWRESPONSE);
		if (start < 0) {
			LOG.error("Error with NEWRESPONSE MOCK command::index=" + start);
			System.out.println("error NewResponse mock, index=" + start);
			return;
		}
		int end = text.indexOf(" ", start + 12);
		if (end < 0) {
			LOG.error("Error with NEWRESPONSE MOCK command::index end=" + end);
			System.out.println("error NewResponse mock, index end=" + end);
			return;
		}

		String nameMock = text.substring(start + 12, end);

		LOG.debug("Mock name to NEW RESPONSE=[" + nameMock + "]");
		LOG.info("Mock name to NEW RESPONSE=[" + nameMock + "]");

		int startFile = text.indexOf("\"");
		String filenameNewResponse = text.substring(startFile + 1, text.length() - 1);
		LOG.debug("Filename for new response=[" + filenameNewResponse + "]");
		LOG.info("Filename for new response=[" + path + filenameNewResponse + "]");

		for (Mock mock : mocks) {
			LOG.info("mock=[" + mock.name + "]");
			if (mock.name.equals(nameMock)) {
				LOG.info("Нашел имя заглушки для NewResponse=" + mock.name);
				mock.fileResponse = filenameNewResponse;
				mock.readResponseFromFile(path);
				return;
			}
		}

		System.out.println("Не нашел имя заглушки для NewResponse=[" + nameMock + "]");
	}

	public String getReceiveText(List<Mock> mocks) throws ScenarioException {
		if (mocks == null) {
			throw new ScenarioException("Mock is null. Может забыли в конфиге прописать?");
		}
		StringBuilder receive = new StringBuilder();
		for (Mock mock : mocks) {
			LOG.info("Request mock=[" + mock.name + "]");
			receive.append(mock.receive);
		}
		LOG.debug("Receive = " + receive.toString());
		return receive.toString();
	}

	public void replaceResponse(List<Mock> mocks, String codeOfStep, String path, HashMap<String, String> vars) throws ScenarioException {
		LOG.info("codeOfStep=" + codeOfStep + "::path=" + path);
		int start = codeOfStep.toUpperCase().indexOf(REPLACE);
		if (start < 0) {
			LOG.error("Error with REPLACE MOCK command::index=" + start);
			System.out.println("error NewResponse mock, index=" + start);
			return;
		}
		int end = codeOfStep.indexOf(" ", start + 8);
		if (end < 0) {
			LOG.error("Error with REPLACE MOCK command::index end=" + end);
			System.out.println("error NewResponse mock, index end=" + end);
			return;
		}

		String nameMock = codeOfStep.substring(start + 8, end);

		LOG.debug("Mock name to REPLACE=[" + nameMock + "]");

		int startFile = codeOfStep.indexOf("\"");
		String fileForReplace = codeOfStep.substring(startFile + 1, codeOfStep.length() - 1);
		LOG.info("Filename for replace=" + path + fileForReplace);

		for (Mock mock : mocks) {
			if (mock.name.equals(nameMock)) {
				LOG.info("Нашел имя заглушки для REPLACE=" + mock.name);
				// читать replace файл
				CommandReplace rep = new CommandReplace();
				String response = rep.getNewMockResponse(path, mock.responseAll, fileForReplace, vars);
				mock.setResponse(response);
				return;
			}
		}

		LOG.warn("Не нашел имя заглушки для REPLACE=[" + nameMock + "]");

	}

	public void repeateResponse(List<Mock> mocks, String codeOfStep, String path, HashMap<String, String> vars, HashMap<String, String> hashMap) throws ScenarioException {
		LOG.info("codeOfStep=" + codeOfStep + "::path=" + path);
		int start = codeOfStep.toUpperCase().indexOf(REPEAT);
		if (start < 0) {
			LOG.error("Error with REPEATE MOCK command::index=" + start);
			System.out.println("error NewResponse mock, index=" + start);
			return;
		}
		int end = codeOfStep.indexOf(" ", start + 8);
		if (end < 0) {
			LOG.error("Error with REPEATE MOCK command::index end=" + end);
			System.out.println("error NewResponse mock, index end=" + end);
			return;
		}

		String nameMock = codeOfStep.substring(start + 8, end);

		LOG.debug("Mock name to REPEATE=[" + nameMock + "]");

		int startFile = codeOfStep.indexOf("\"");
		String fileForReplace = codeOfStep.substring(startFile + 1, codeOfStep.length() - 1);
		LOG.info("Filename for repeate=" + path + fileForReplace);

		for (Mock mock : mocks) {
			if (mock.name.equals(nameMock)) {
				LOG.info("Нашел имя заглушки для REPEATE=" + mock.name);
				CommandRepeate rep = new CommandRepeate();
				String response;
				try {
					response = rep.getNewMockResponse(path, mock, fileForReplace, vars, hashMap);
				} catch (ReadFileException e) {
					throw new ScenarioException("error in read file to new response for MOCK::" + e.getMessage());
				}
				mock.setResponse(response);
				return;
			}
		}
		throw new ScenarioException("Не нашел имя заглушки для REPEATE=[" + nameMock + "]");
	}
}
