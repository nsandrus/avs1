package ru.nsandrus.command;

import static ru.nsandrus.entity.KeyWords.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import ru.nsandrus.avs.MQJson;
import ru.nsandrus.entity.JmsMessage;
import ru.nsandrus.entity.MQueueEntity;
import ru.nsandrus.entity.ResultEntitySoap;
import ru.nsandrus.exeptions.ScenarioException;
import ru.nsandrus.file.ReadFileException;



public class CommandSendJms {

	Logger LOG = Logger.getLogger(CommandSendJms.class);
	String commandLine;
	String response;
	private String fileSend;
	private String file2Replace;
	public String path;
	private String queueNameList ="";
	MQueueEntity mqueue;

	JmsMessage jmsObject;
	private List<MQueueEntity> queues;

	@Override
	public String toString() {
		return "CommandSendJms [commandLine=" + commandLine + ", response=" + response + ", fileSend=" + fileSend + ", file2Replace=" + file2Replace + ", path=" + path + ", queueName=" + queueNameList
				+ ", jmsObject=" + jmsObject + "]";
	}

	public CommandSendJms(String path, String text, List<MQueueEntity> mqueues) {
		this.path = path;
		this.commandLine = text;
		this.queues = mqueues;
		LOG.info("Constructor=" + this);
	}


	public ResultEntitySoap exec(HashMap<String, String> hashMap) throws ReadFileException, ScenarioException {
		ResultEntitySoap result = new ResultEntitySoap();
		if (isSecondCommandReplace()) {
			if (!commandSendJmsReplace()) {
				String mes = "Ошибка в команде REPLACE::" + this;
				LOG.error(mes);
				result.isOk = false;
				result.error = mes;
				return result;
			}
		} else if (isSecondCommandRepeate()){
			commandSendJmsRepeate(hashMap);
		} else {
			commandSendJms();
		}
		result.result = execSendJms();
		result.isOk = true;
		return result;
	}

	private boolean isSecondCommandRepeate() {
		String[] commands = commandLine.split(" ");
		if (commands[1].equalsIgnoreCase(REPEAT)) {
			return true;
		}
		return false;
	}


	private boolean isSecondCommandReplace() {
		String[] commands = commandLine.split(" ");
		if (commands[1].equalsIgnoreCase(REPLACE)) {
			return true;
		}
		return false;
	}

	private boolean commandSendJmsReplace() throws ScenarioException, ReadFileException {
		LOG.info("Command::SendJms REPLACE::");
		// isReplace = true;
		String[] nameQ = commandLine.split(" ");
		queueNameList = nameQ[2];

		String[] files = commandLine.split("\"");

		fileSend = files[1];
		file2Replace = files[3];

		LOG.info("file1=" + fileSend + "::file2=" + file2Replace);
		jmsObject = getSoapFromFile(fileSend);
		
		for (MQueueEntity queue : queues) {
			LOG.trace("List cfg queue=" + queue);
			if (queue.queueNameList.equals(queueNameList)) {
				LOG.info("Found queueInCfg["+queueNameList+"]");
				// данные впихнуть
				mqueue = queue;
				mqueue.message = jmsObject.message;
			}
		}
		
		LOG.info("Send Jms message in queueNameList["+queueNameList+"]from file=" + fileSend + "::Data=" + jmsObject);
		CommandReplace replace = new CommandReplace();
		mqueue.message = replace.getReplaced(path, fileSend, file2Replace);
		return true;
	}

	private boolean commandSendJmsRepeate(HashMap<String, String> hashMap) throws ScenarioException, ReadFileException {
		LOG.info("Command::SendJms REPEATE::");
		String[] nameQ = commandLine.split(" ");
		queueNameList = nameQ[2];

		String[] files = commandLine.split("\"");

		fileSend = files[1];
		file2Replace = "";

		LOG.info("file1=" + fileSend + "::file2=" + file2Replace);
		jmsObject = getSoapFromFile(fileSend);
		
		for (MQueueEntity queue : queues) {
			LOG.trace("List cfg queue=" + queue);
			if (queue.queueNameList.equals(queueNameList)) {
				LOG.info("Found queueInCfg["+queueNameList+"]");
				// данные впихнуть
				mqueue = queue;
				mqueue.message = jmsObject.message;
			}
		}
		
		LOG.info("Send Jms message in queueNameList["+queueNameList+"]from file=" + fileSend + "::Data=" + jmsObject);
		
		CommandRepeate repeate = new CommandRepeate();
		mqueue.message = repeate.getRepeate(path, fileSend, hashMap);
		return true;
	}

	private void commandSendJms() throws ReadFileException {
		LOG.info("SendJms ");
		String[] nameQ = commandLine.split(" ");
		queueNameList = nameQ[1];
		String[] files = commandLine.split("\"");
		LOG.debug("line code=" + Arrays.toString(files));

		fileSend = files[1];
		jmsObject = getSoapFromFile(fileSend);

		for (MQueueEntity queue : queues) {
			LOG.trace("List cfg queue=" + queue);
			if (queue.queueNameList.equals(queueNameList)) {
				LOG.info("Found queueInCfg["+queueNameList+"]");
				mqueue = queue;
				mqueue.message = jmsObject.message;
			}
		}
		LOG.info("Send Jms message in queueNameList["+queueNameList+"]from file=" + fileSend + "::Data=" + jmsObject);
	}

	private String execSendJms() throws ScenarioException {
		if (mqueue == null ) {
			throw new ScenarioException("mqueue is null!!!");
		}
		MQJson scEmulator = new MQJson(mqueue);
		LOG.debug("Try send JMS::" + mqueue);
		scEmulator.SendMessage();
		return jmsObject.response;
	}

	private JmsMessage getSoapFromFile(String filename) throws ReadFileException {

		File file = new File(path + filename);
		String data = "";
		try {
			data = FileUtils.readFileToString(file, "UTF-8");
		} catch (IOException e) {
			String textError = "Error read message from file " + file.getAbsolutePath() + "\n\t" + e.getMessage();
			LOG.error(textError);
			data = "NO FILE FOUND!";
			throw new ReadFileException(textError);
		}
		JmsMessage jms = new JmsMessage(data);
		LOG.debug("JMS=" + jms);
		return jms;
	}

}
