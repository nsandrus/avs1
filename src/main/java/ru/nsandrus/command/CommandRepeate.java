package ru.nsandrus.command;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import ru.nsandrus.entity.SoapMessage;
import ru.nsandrus.exeptions.ScenarioException;
import ru.nsandrus.file.ReadFileException;
import ru.nsandrus.mock.Mock;

public class CommandRepeate {

	Logger LOG = Logger.getLogger(CommandRepeate.class);
	public String path;
	public HashMap<String, String> vars;

	public String getNewMockResponse(String path, Mock mock, String fileForReplace, HashMap<String, String> vars, HashMap<String, String> map) throws ScenarioException, ReadFileException {
		this.path = path;
		this.vars = vars;

		LOG.debug("MOCK=" + mock.responseAll + "::path=" + path + "::filename=" + fileForReplace);
		LOG.debug("MAP=" + map);
		mock.fileResponse = fileForReplace;
		mock.readResponseFromFile(path);
		LOG.debug("NewResponse=" + mock.responseAll + "::path=" + path + "::fileReplace=" + fileForReplace);
		String response = mock.responseAll;

		for (Entry<String, String> pattern : map.entrySet()) {
			response = response.replaceAll(pattern.getKey(), Matcher.quoteReplacement(pattern.getValue()));
		}
		LOG.debug("Repeated response=" + response);
		return response;
	}

	public SoapMessage exec(String path, String fileSend, HashMap<String, String> map) throws ScenarioException {
		this.path = path;

		SoapMessage soapObject = getSoapFromFile(fileSend);
		LOG.debug("READ::" + soapObject);
		LOG.debug("MAP=" + map);
		for (Entry<String, String> pattern : map.entrySet()) {
			// System.out.println("key=" + pattern.getKey() + "::value=" + pattern.getValue());
			soapObject.message = soapObject.message.replaceAll(pattern.getKey(), Matcher.quoteReplacement(pattern.getValue()));
		}
		return soapObject;
	}

	private SoapMessage getSoapFromFile(String filename) throws ScenarioException {
		File file = new File(path + filename);
		String data = "";
		try {
			data = FileUtils.readFileToString(file, "UTF-8");
		} catch (IOException e) {
			String errorText = "Error read message from file " + file.getAbsolutePath() + "\n" + e.getMessage();
			LOG.error(errorText);
			System.out.println(errorText);
			throw new ScenarioException(errorText);
		}
		SoapMessage soap = new SoapMessage(data, vars);
		LOG.debug("SOAP=" + soap + "\n::vars=" + vars);
		return soap;
	}

	private String getSoapFromFileClear(String filename) throws ScenarioException {
		File file = new File(path + filename);
		String data = "";
		try {
			data = FileUtils.readFileToString(file, "UTF-8");
		} catch (IOException e) {
			String errorText = "Error read message from file " + file.getAbsolutePath() + "\n" + e.getMessage();
			LOG.error(errorText);
			System.out.println(errorText);
			throw new ScenarioException(errorText);
		}
		LOG.debug("read data=" + data);
		return data;
	}

	public String getRepeate(String path2, String fileSend, HashMap<String, String> map) throws ScenarioException {
		this.path = path2;
		String soapObject = getSoapFromFileClear(fileSend);
		LOG.debug("READ::" + soapObject);
		LOG.debug("MAP=" + map);
		for (Entry<String, String> pattern : map.entrySet()) {
			// System.out.println("key=" + pattern.getKey() + "::value=" + pattern.getValue());
			soapObject = soapObject.replaceAll(pattern.getKey(), Matcher.quoteReplacement(pattern.getValue()));
		}
		return soapObject;
	}
}
