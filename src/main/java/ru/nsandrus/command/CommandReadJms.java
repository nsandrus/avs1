package ru.nsandrus.command;

import static ru.nsandrus.entity.KeyWords.*;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import ru.nsandrus.avs.MQJson;
import ru.nsandrus.entity.JmsMessage;
import ru.nsandrus.entity.MQueueEntity;
import ru.nsandrus.entity.ResultEntitySoap;
import ru.nsandrus.exeptions.ScenarioException;
import ru.nsandrus.file.ReadFileException;



public class CommandReadJms {

	Logger LOG = Logger.getLogger(CommandReadJms.class);
	String commandLine;
	String response;
	private String fileSend;
	private String file2Replace;
	public String path;
	private String queueNameList ="";
	MQueueEntity mqueue;

	JmsMessage jmsObject;
	private List<MQueueEntity> queues;
	private String nameQueue;




	@Override
	public String toString() {
		return "CommandSendJms [commandLine=" + commandLine + ", response=" + response + ", fileSend=" + fileSend + ", file2Replace=" + file2Replace + ", path=" + path + ", queueName=" + queueNameList
				+ ", jmsObject=" + jmsObject + "]";
	}



	public CommandReadJms(List<MQueueEntity> mqueues) {

		
//		this.nameQueue = nameQueue;
		this.queues = mqueues;

		LOG.info("Constructor=" + this);
	}



	public ResultEntitySoap exec() throws ReadFileException, ScenarioException {
		ResultEntitySoap result = new ResultEntitySoap();
		if (isSecondCommandReplace()) {
			if (!commandSendJmsReplace()) {
				String mes = "Ошибка в команде REPLACE::" + this;
				LOG.error(mes);
				result.isOk = false;
				result.error = mes;
				return result;
			}
		} else {
			commandSendJms();
		}
		result.result = execSendJms();
		result.isOk = true;
		return result;
	}

	private boolean isSecondCommandReplace() {
		String[] commands = commandLine.split(" ");
		if (commands[1].equalsIgnoreCase(REPLACE)) {
			return true;
		}
		return false;
	}

	private boolean commandSendJmsReplace() throws ScenarioException {
		LOG.info("Command::SendJms REPLACE::");
		// isReplace = true;
		String[] files = commandLine.split("\"");

		fileSend = files[1];
		file2Replace = files[3];

		LOG.info("file1=" + fileSend + "::file2=" + file2Replace);

//		CommandReplace replace = new CommandReplace();
//		jmsObject = replace.exec(path, fileSend, file2Replace);
		response = execSendJms();
		if (response != null)   // это теперь не понадобится по идее
			return true;
		else
			return false;
	}

	private void commandSendJms() throws ReadFileException {
		LOG.info("SendJms ");
		// isReplace = true;
		String[] nameQ = commandLine.split(" ");
		queueNameList = nameQ[1];
		String[] files = commandLine.split("\"");
		LOG.debug("line code=" + Arrays.toString(files));

		fileSend = files[1];
		jmsObject = getSoapFromFile(fileSend);

		for (MQueueEntity queue : queues) {
			LOG.trace("List cfg queue=" + queue);
			if (queue.queueNameList.equals(queueNameList)) {
				LOG.info("Found queueInCfg["+queueNameList+"]");
				// данные впихнуть
				mqueue = queue;
				mqueue.message = jmsObject.message;
			}
		}
		LOG.info("Send Jms message in queueNameList["+queueNameList+"]from file=" + fileSend + "::Data=" + jmsObject);

	}

	private String execSendJms() throws ScenarioException {
		// послать само сообещние через http и получить ответ

//		Hashtable<String, String> props = new Hashtable<String, String>();
//		 props.put("operation","sendSurvey");
//		 String queueName="TEST_1.LQ";
//		 String queueName="atc.beepayxp";
//		 atc.beepayxp
//		LOG.debug("Подготовка данных " );
//		props.put("event_type", "ACT0");

//		MQueue queue = new MQueue();
//		queue = getMQueueFromConfig();
//		queue.queueName = "atc.beepayxp";
//		queue.message = jmsObject.message;
		MQJson scEmulator = new MQJson(mqueue);

		LOG.debug("Try send JMS::" + mqueue);

		scEmulator.SendMessage();
//		System.out.println("Finished");
//		jmsObject.response = // запрос JMS

//		LOG.info("Response=" + jmsObject.response);
		return jmsObject.response;
	}

	private JmsMessage getSoapFromFile(String filename) throws ReadFileException {

		File file = new File(path + filename);
		String data = "";
		try {
			data = FileUtils.readFileToString(file, "UTF-8");
		} catch (IOException e) {
			String textError = "Error read message from file " + file.getAbsolutePath() + "\n\t" + e.getMessage();
			LOG.error(textError);
//			System.out.println(textError);
			data = "NO FILE FOUND!";
			throw new ReadFileException(textError);
		}
		JmsMessage jms = new JmsMessage(data);
		LOG.debug("JMS=" + jms);
		return jms;
	}



	public String getAllMessages(String nameQueue) throws ScenarioException {
		
		MQueueEntity resultQueue = null;
		for ( MQueueEntity queue:queues) {
			
			if (queue.queueNameList.equals(nameQueue)) {
				resultQueue = queue;
				LOG.info("found JMS queue=" + nameQueue);
				break;
			}
		}
		
		if (resultQueue == null) {
			throw new ScenarioException("Not found queue [" + nameQueue + "]");
		}
		
		MQJson scEmulator = new MQJson(resultQueue);
		LOG.debug("Try read JMS::" + resultQueue);
		String message = scEmulator.ReadMessage();
		return message;
	}

}
