package ru.nsandrus.command;

import static ru.nsandrus.entity.KeyWords.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.nsandrus.entity.SoapMessage;
import ru.nsandrus.exeptions.ScenarioException;

public class CommandReplace {

	Logger LOG = Logger.getLogger(CommandReplace.class);
	public String path;
	public HashMap<String, String> vars;

	public String getNewMockResponse(String path, String response, String file2Replace, HashMap<String, String> vars) throws ScenarioException {
		this.path = path;
		this.vars = vars;

		LOG.debug("Response=" + response + "::path="+ path+"::fileReplace=" + file2Replace);
		Map<String, Object> map = readJsonMap(file2Replace);

		LOG.debug("MAP=" + map);

		for (Entry<String, Object> pattern : map.entrySet()) {
			if (pattern.getKey().equals(REPLACE)) {
				HashMap<String, String> hmap = (HashMap<String, String>) pattern.getValue();
				for (Entry<String, String> key : hmap.entrySet()) {
					response = response.replaceAll(key.getKey(), Matcher.quoteReplacement(key.getValue()));
				}
			}
		}
		LOG.debug("Replaced response=" + response);
		return response;
	}


	public SoapMessage exec(String path, String fileSend, String file2Replace, HashMap<String, String> vars2) throws ScenarioException {
		this.path = path;
		this.vars = vars2;
		// читаем первый файл и меняем в нем по ключам из второго файла.
		SoapMessage soapObject = getSoapFromFile(fileSend);
		LOG.debug("READ::" + soapObject);
		Map<String, Object> map = readJsonMap(file2Replace);
		LOG.debug("MAP=" + map);
		for (Entry<String, Object> pattern : map.entrySet()) {
			if (pattern.getKey().equals(REPLACE)) {
				HashMap<String, String> hmap = (HashMap<String, String>) pattern.getValue();
				for (Entry<String, String> key : hmap.entrySet()) {
					soapObject.message = soapObject.message.replaceAll(key.getKey(), Matcher.quoteReplacement(key.getValue()));
				}
			}
		}
		return soapObject;
	}

	private SoapMessage getSoapFromFile(String filename) throws ScenarioException {
		File file = new File(path + filename);
		String data = "";
		try {
			data = FileUtils.readFileToString(file, "UTF-8");
		} catch (IOException e) {
			String errorText = "Error read message from file " + file.getAbsolutePath() + "\n" + e.getMessage();
//			LOG.error(errorText);
//			System.out.println(errorText);
			throw new ScenarioException(errorText);
		}
		SoapMessage soap = new SoapMessage(data,vars);
		LOG.debug("SOAP=" + soap + "\n::vars=" + vars);
		return soap;
	}

	private Map<String, Object> readJsonMap(String filename) throws ScenarioException {
		File file = new File(path + filename);
		String data = "";
		try {
			data = FileUtils.readFileToString(file, "UTF-8");
		} catch (IOException e) {
			String textError = "Error read message from file " + file.getAbsolutePath() + "\n" + e.getMessage();
//			LOG.error(textError);
//			System.out.println(textError);
			throw new ScenarioException(textError);
		}
		Map<String, Object> replaceMap = null;
		try {
			replaceMap= jsonString2Map(data);
		} catch (JSONException e) {
//			System.out.println("Ошибка в формате JSON файла." + e.getMessage());
			throw new ScenarioException("Ошибка в формате JSON файла." + e.getMessage());
		}

		return replaceMap;
	}

	public static Map<String, Object> jsonString2Map(String jsonString) throws JSONException {
		Map<String, Object> keys = new HashMap<String, Object>();

		org.json.JSONObject jsonObject = new org.json.JSONObject(jsonString); // HashMap
		Iterator<?> keyset = jsonObject.keys(); // HM

		while (keyset.hasNext()) {
			String key = (String) keyset.next();
			Object value = jsonObject.get(key);
			if (value instanceof org.json.JSONObject) {
				keys.put(key, jsonString2Map(value.toString()));
			} else {
				keys.put(key, value);
			}
		}
		return keys;
	}
	
	public static ArrayList<HashMap<String, String>> jsonString2Array(String jsonString) throws JSONException, ScenarioException {
		JSONArray json = new org.json.JSONArray(jsonString); // HashMap
		
		try {
		     ArrayList<HashMap<String,String>> list = (ArrayList<HashMap<String,String>>) toList(json);
		     return list;
		} catch (JSONException e) {
		     throw new ScenarioException("Error: can not fornat JSONarray to ArrayList::" + jsonString);
		}
	}
	
	private static Object fromJson(Object json) throws JSONException {
	    if (json == JSONObject.NULL) {
	        throw new JSONException("it's not JSONObject, but NULL");
	    } else if (json instanceof JSONObject) {
	        return jsonToMap((JSONObject) json);
	    } else if (json instanceof JSONArray) {
	        return toList((JSONArray) json);
	    } else {
	        return json;
	    }
	}
	public static Map<String, String> jsonToMap(JSONObject object) throws JSONException {
	    Map<String, String> map = new HashMap<>();
	    Iterator<String> keys = object.keys();
	    while (keys.hasNext()) {
	        String key = (String) keys.next();
	        map.put(key, fromJson(object.get(key)).toString());
	    }
	    return map;
	}
	private static List toList(JSONArray array) throws JSONException {
	    List list = new ArrayList<>();
	    int size = array.length();
	    for (int i = 0; i < size; i++) {
	        list.add(fromJson(array.get(i)));
	    }
	    return list;
	}


	public String getReplaced(String path2, String fileSend, String file2Replace) throws ScenarioException {
		this.path = path2;
//		this.vars = vars2;
		// читаем первый файл и меняем в нем по ключам из второго файла.
		String soapObject = getSoapFromFileClear(fileSend);
		LOG.debug("READ::" + soapObject);
		Map<String, Object> map = readJsonMap(file2Replace);
		LOG.debug("MAP=" + map);
		for (Entry<String, Object> pattern : map.entrySet()) {
			if (pattern.getKey().equals(REPLACE)) {
				HashMap<String, String> hmap = (HashMap<String, String>) pattern.getValue();
				for (Entry<String, String> key : hmap.entrySet()) {
					soapObject = soapObject.replaceAll(key.getKey(), Matcher.quoteReplacement(key.getValue()));
				}
			}
		}
		return soapObject;
	}

	private String getSoapFromFileClear(String filename) throws ScenarioException {
		File file = new File(path + filename);
		String data = "";
		try {
			data = FileUtils.readFileToString(file, "UTF-8");
		} catch (IOException e) {
			String errorText = "Error read message from file " + file.getAbsolutePath() + "\n" + e.getMessage();
//			LOG.error(errorText);
//			System.out.println(errorText);
			throw new ScenarioException(errorText);
		}
		
		LOG.debug("read data=" + data);
		return data;
	}
}
