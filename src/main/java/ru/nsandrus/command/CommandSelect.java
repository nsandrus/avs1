package ru.nsandrus.command;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import ru.nsandrus.entity.DbEntity;
import ru.nsandrus.exeptions.ScenarioException;

public class CommandSelect {

	private String DB_DRIVER;
	private String DB_CONNECTION;
	private String DB_USER;
	private String DB_PASSWORD;

	Logger LOG = Logger.getLogger(CommandSelect.class);
	DbEntity dataBase;

	private List<DbEntity> databases;
	private String nameDb;
	private String filename;
	private String path;

	public CommandSelect(List<DbEntity> dbnames, String nameDB, String path) {
		this.databases = dbnames;
		this.nameDb = nameDB;
		this.path = path;
		LOG.info("Constructor=" + this);
	}

	@Override
	public String toString() {
		return "CommandSelect [dataBase=" + dataBase + ", databases=" + databases + ", nameDb=" + nameDb + ", filename="
				+ filename + "]";
	}

	public String select(String filename) throws ScenarioException {

		String resultSelect = "NO SELECT YET!";
		for (DbEntity db : databases) {
			if (db.dataBaseNameList.equals(nameDb)) {
				DB_DRIVER = db.dbDriver;
				DB_CONNECTION = db.dbConnection;
				DB_USER = db.dbUser;
				DB_PASSWORD = db.dbPassword;
			}
		}
		if (DB_DRIVER == null) {
			throw new ScenarioException("DB_DRIVER is null. Name DB=[" + nameDb + "]");
		}
		String select = getSqlFromFile(filename);
		LOG.debug("clear SELECT = [" + select + "]");

		try {
			resultSelect = selectRecordsFromTable(select);
		} catch (SQLException e) {
			throw new ScenarioException("SQL Exception::" + e.getMessage());
		}
		return resultSelect;
	}

	private String getSqlFromFile(String filename2) throws ScenarioException {
		String data = readFile(filename2);
		String[] lines = data.split("\n");
		StringBuilder clearSql = new StringBuilder();
		for (String line : lines) {
			if (line.trim().startsWith("--")) {
				continue;
			}
			clearSql.append(line + " ");
		}
		return clearSql.toString();
	}

	private String readFile(String filename2) throws ScenarioException {
		File file = new File(path + filename2);
		String data = "";
		try {
			data = FileUtils.readFileToString(file, "UTF-8");
		} catch (IOException e) {
			String errorText = "Error read sql from file " + file.getAbsolutePath() + "\n" + e.getMessage();
			LOG.error(errorText);
			throw new ScenarioException(errorText);
		}
		return data;
	}

	private String selectRecordsFromTable(String select) throws SQLException, ScenarioException {

		StringBuilder resultSelect = new StringBuilder();
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		String selectSQL = select;

		try {
			dbConnection = getDBConnection();
			if (dbConnection == null) {
				throw new ScenarioException("Error connecting to " + DB_CONNECTION);
			}
			preparedStatement = dbConnection.prepareStatement(selectSQL);
			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next()) {
				int col = rs.getMetaData().getColumnCount();
				LOG.debug("Num of colums=" + col);
				for (int i = 1; i <= col; i++) {
					resultSelect.append(rs.getString(i) + "\t");
					// LOG.debug("Current index=" + i);
				}
				resultSelect.append("\n");
			}
			 LOG.debug("Result select::" + resultSelect.toString());

		} catch (SQLException e) {

			throw new ScenarioException("SQLException while SELECT::" + e);

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}
		return resultSelect.toString();
	}

	private Connection getDBConnection() throws ScenarioException {
		Connection dbConnection = null;
		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			throw new ScenarioException(
					"SQL error DB_DRIVER=" + DB_DRIVER + "::Check driver jar in lib folder::" + e);
		}
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			return dbConnection;
		} catch (SQLException e) {
			throw new ScenarioException("SQL error connection::" + DB_CONNECTION + "::" + e.getMessage());
		}

	}

}
