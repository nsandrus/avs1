package ru.nsandrus.command;

import static ru.nsandrus.entity.KeyWords.*;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import ru.nsandrus.avs.HttpSoap;
import ru.nsandrus.entity.ResultEntitySoap;
import ru.nsandrus.entity.SoapMessage;
import ru.nsandrus.exeptions.ScenarioException;

public class CommandSendSoap {

	Logger LOG = Logger.getLogger(CommandSendSoap.class);
	String commandLine;
	String response;
	private String fileSend;
	private String file2Replace;
	public String path;
	private int timeOut;

	SoapMessage soapObject;
	private HashMap<String, String> vars;
	private HashMap<String, String> hashMap;

	public CommandSendSoap(String path, String text, int timeOut, HashMap<String, String> vars) {

		this.path = path;
		this.commandLine = text;
		this.timeOut = timeOut;
		this.vars = vars;
		LOG.info("Constructor=" + this + "::vars=" + vars);
	}

	public ResultEntitySoap getResultSoapRequest(HashMap<String, String> hashMap) throws ScenarioException {
		this.hashMap = hashMap;
		ResultEntitySoap result = new ResultEntitySoap();
		if (isSecondCommandReplace()) {
			try {
				commandSendSoapReplace();
			} catch (ScenarioException e) {
				result.isOk = false;
				throw new ScenarioException("Ошибка в команде REPLACE::" + e.getMessage() + "\n\t" + this);
			}

		} else if (isSecondCommandRepeate()) {
			commandSendSoapRepeate(hashMap);
		} else {
			commandSendSoap();
		}
		result.result = execSendSoap();
		result.isOk = true;
		return result;
	}

	private boolean isSecondCommandRepeate() {
		LOG.info("cheked is REPEATE");
		String[] commands = commandLine.split(" ");
		return (commands[1].equalsIgnoreCase(REPEAT)) ? true : false;
	}

	private boolean isSecondCommandReplace() {
		String[] commands = commandLine.split(" ");
		return (commands[1].equalsIgnoreCase(REPLACE)) ? true : false;
	}

	private boolean commandSendSoapReplace() throws ScenarioException {
		String[] files = commandLine.split("\"");
		LOG.info("SendSoap REPLACE::" + files.toString());
		if (files.length < 3) {
			throw new ScenarioException("\tError:no file in argument command REPLACE");
		}
		fileSend = files[1];
		file2Replace = files[3];
		LOG.info("file1=" + fileSend + "::file2=" + file2Replace);
		CommandReplace replace = new CommandReplace();
		soapObject = replace.exec(path, fileSend, file2Replace, vars);
		return isSendSuccess();
	}

	private boolean isSendSuccess() throws ScenarioException {
		response = execSendSoap();
		return (response != null) ? true : false;
	}

	private boolean commandSendSoapRepeate(HashMap<String, String> hashMap2) throws ScenarioException {
		String[] files = commandLine.split("\"");
		LOG.info("Command REPEATE::" + files.toString());
		if (files.length < 2) {
			throw new ScenarioException("\tError:no file in argument command REPEATE");
		}
		fileSend = files[1];
		file2Replace = "not used in repeate!";
		LOG.info("file1=" + fileSend + "::file2=" + file2Replace);
		CommandRepeate repeate = new CommandRepeate();
		soapObject = repeate.exec(path, fileSend, hashMap);
		return isSendSuccess();
	}

	private void commandSendSoap() throws ScenarioException {
		String[] files = commandLine.split("\"");
		LOG.info("command SendSOAP::" + files.toString());
		fileSend = files[1];
		soapObject = getSoapFromFile(fileSend);
		LOG.info("Send SOAP request from file=" + fileSend + "::Data=" + soapObject);
	}

	private String execSendSoap() throws ScenarioException {
		if (soapObject == null) {
			return "null if found in execSendSoap";
		}
		HttpSoap httpSoap = new HttpSoap();
		soapObject.response = httpSoap.postDataAuthorize(soapObject, timeOut);
		LOG.info("Response=" + soapObject.response);
		return soapObject.response;
	}

	private SoapMessage getSoapFromFile(String filename) throws ScenarioException {
		File file = new File(path + filename);
		String data = "";
		try {
			data = FileUtils.readFileToString(file, "UTF-8");
		} catch (IOException e) {
			throw new ScenarioException(
					"Error read message from file " + file.getAbsolutePath() + "\n" + e.getMessage());
		}
		LOG.debug("before vars=" + vars);
		SoapMessage soap = new SoapMessage(data, vars);
		LOG.debug("SOAP=" + soap + "::vars=" + vars);
		return soap;
	}

	@Override
	public String toString() {
		return "CommandSendSoap [commandLine=" + commandLine + ", response=" + response + ", fileSend=" + fileSend
				+ ", file2Replace=" + file2Replace + ", path=" + path + ", soapObject=" + soapObject + "]";
	}

}
