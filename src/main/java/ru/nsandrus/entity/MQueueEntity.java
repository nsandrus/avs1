package ru.nsandrus.entity;

import java.util.Hashtable;

public class MQueueEntity {

	public int port;
	public String hostName;
	public String queueManager;
	public String channel;
	public String user;
	public String password;
	public String queueName;
	public String queueNameList;
	public String message;

	public Hashtable<String, String> props = new Hashtable<String, String>();

	@Override
	public String toString() {
		return "MQueue [port=" + port + ", hostName=" + hostName + ", queueManager=" + queueManager + ", channel=" + channel + ", user=" + user + ", password=" + password + ", queueName=" + queueName
				+ ", queueNameList=" + queueNameList + ", message=" + message + ", props=" + props + "]";
	}
}
