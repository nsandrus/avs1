package ru.nsandrus.entity;

import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import ru.nsandrus.exeptions.ScenarioException;

public class SoapMessage {

	Logger LOG = Logger.getLogger(SoapMessage.class);

	public String url;
	public String login;
	public String pass;
	public String message;
	public String response;

	public String contentType;

	public SoapMessage(String data, HashMap<String, String> vars) throws ScenarioException {
		LOG.trace("incoming data=" + data + "::vars=" + vars);
		String[] lines = data.split("\n");
		for (String line : lines) {
			if (line.toUpperCase().startsWith("URL=")) {
				getUrl(vars, line);
			}
			if (line.toUpperCase().startsWith("CONTENT-TYPE=")) {
				contentType = line.trim().substring(13);
			}
			if (line.toUpperCase().startsWith("LOGIN=")) {
				login = line.trim().substring(6);
			}
			if (line.toUpperCase().startsWith("PASS=")) {
				pass = line.trim().substring(5);
			}
		}
		int start = data.indexOf("XML=");
		if (start < 0) {
			throw new ScenarioException("Not found tag 'XML=' in SOAP message.");
		}
		message = data.substring(start + 4);
	}

	
	protected void getUrl(HashMap<String, String> vars, String line) throws ScenarioException {
		LOG.trace("SOAP URL=" + url + "::vars=" + vars);
		url = line.trim().substring(4);
		boolean isFound = false;
		if (vars != null) {
			for (Entry<String, String> urlVars : vars.entrySet()) {
				LOG.trace("One VAR URL=" + urlVars.toString());
				if (urlVars.getKey().equalsIgnoreCase(url)) {
					url = urlVars.getValue();
					isFound = true;
					LOG.trace("VARS URL=" + url);
				}
			}
		}
		if (!isFound) {
			if (url.trim().toUpperCase().startsWith("HTTP:/") | url.trim().toUpperCase().startsWith("HTTPS:/")) {
				;
			} else {
				throw new ScenarioException("Can't find URL for send SOAP request.URL=" + url);
			}
		}

	}

	@Override
	public String toString() {
		return "SoapMessage [LOG=" + LOG + ", url=" + url + ", login=" + login + ", pass=" + pass + ", message=" + message + ", response=" + response + "]";
	}

}
