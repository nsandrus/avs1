package ru.nsandrus.entity;

import java.util.ArrayList;
import java.util.List;

public class KeyWords {

	public static final String TEXT = "TEXT";
	public static final String SET = "SET";
	public static final String COMMENT = "//";
	public static final String MOCK = "MOCK";
	public static final String WAIT = "WAIT";
	public static final String DISABLE_CHAR = "-";
	public static final String MOCKREPLACE = "MOCKREPLACE";
	public static final String PLAN = "PLAN";
	public static final String SHUTDOWNCOMPONENT = "SHUTDOWNCOMPONENT";
	public static final String STARTCOMPONENT = "STARTCOMPONENT";
	public static final String SENDHTTP = "SENDHTTP";
	public static final String SENDJMS = "SENDJMS";
	public static final String SENDSOAP = "SENDSOAP";
	public static final String ASSERTLOG = "ASSERTLOG";
	public static final String ASSERTMOCK = "ASSERTMOCK";
	public static final String ASSERTJMS = "ASSERTJMS";
	public static final String ASSERTSQL = "ASSERTSQL";
	public static final String ASSERTGF = "ASSERTGF";
	public static final String ASSERT = "ASSERT";
	public static final String SCENARIOFOR = "SCENARIOFOR";
	public static final String SCENARIO = "SCENARIO";
	public static final String START = "START";
	public static final String NEWRESPONSE = "NEWRESPONSE";
	public static final String STOP = "STOP";
	public static final String REPLACE = "REPLACE";
	public static final String REPEAT = "REPEATE";
	public static final String HTTP_TIMEOUT = "HTTPTIMEOUT";
	public static final String TESTPLAN = "#TEST PLAN#";
	
	
	
	static List<String> words;
	
	public static List<String> getList() {
		if (words == null) {
			createInstance();
		}
		return words;
	}

	private KeyWords() {
	}
	
	private static void createInstance() {
		words = new ArrayList<>();
		
		words.add(SCENARIO);
		words.add(SCENARIOFOR);
		words.add(ASSERT);
		words.add(ASSERTMOCK);
		words.add(ASSERTLOG);
		words.add(ASSERTGF);
		words.add(SENDSOAP);
		words.add(SENDJMS);
		words.add(SENDHTTP);
		words.add(STARTCOMPONENT);
		words.add(SHUTDOWNCOMPONENT);
		words.add(PLAN);
		words.add(MOCKREPLACE);
		words.add(DISABLE_CHAR);
		words.add(WAIT);
		words.add(MOCK);
		words.add(COMMENT);
		words.add(SET);
		words.add(TEXT);
		words.add(REPEAT);
		words.add(REPLACE);
		words.add(TESTPLAN);
	}

	public static boolean isValidCommand(String firstCommand) {
		if (words == null) {
			createInstance();
		}
		for (String word : words) {
			if (word.trim().equalsIgnoreCase(firstCommand)) {
				return true;
			}
		}
		return false;
	}

}
