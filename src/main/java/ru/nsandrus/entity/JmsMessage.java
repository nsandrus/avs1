package ru.nsandrus.entity;

import org.apache.log4j.Logger;

public class JmsMessage {

	Logger LOG = Logger.getLogger(JmsMessage.class);

	public String url;
	public String login;
	public String pass;
	public String message;
	public String response;


	public JmsMessage(String data) {
		LOG.debug("incoming data=" + data);
		message = data;

	}


	@Override
	public String toString() {
		return "SoapMessage [LOG=" + LOG + ", url=" + url + ", login=" + login + ", pass=" + pass + ", message=" + message + ", response=" + response + "]";
	}



}
