package ru.nsandrus.entity;

import java.util.ArrayList;
import java.util.List;

public class LogString {

	public int id;
	public String scenarioName="";
	public long timeWork;
	public String scenarioException = "";
	public List<String> assertsPassed = new ArrayList<>();
	public List<AssertFailed> assertsFailed = new ArrayList<>();
	public long startTime;
	
	@Override
	public String toString() {
		return "LogString [id=" + id + ", scenarioName=" + scenarioName + ", timeWork=" + timeWork
				+ ", scenarioException=" + scenarioException + ", Passed=" + assertsPassed + ", \n\tFailed="
				+ assertsFailed + ", startTime=" + startTime + "]";
	}
	
	
	

	
	

	

}
