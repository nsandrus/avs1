package ru.nsandrus.file;

public class ReadFileException extends Exception {

	private String textException="";
	/**
	 * 
	 */
	private static final long serialVersionUID = -2359066862358454093L;

	public ReadFileException(String string) {
		System.out.println(string);
		textException = string;
	}

	 public String getMessage() {
	        return textException;
	    }
}
