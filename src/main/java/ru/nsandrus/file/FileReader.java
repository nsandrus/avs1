package ru.nsandrus.file;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import ru.nsandrus.exeptions.ScenarioException;

public class FileReader {

	static Logger LOG = Logger.getLogger(FileReader.class);

	public static String readFile(String path) throws ReadFileException {

		File file = new File(path);
		String data = "";
		try {
			data = FileUtils.readFileToString(file, "UTF-8");
		} catch (IOException e) {
			LOG.error("Error::" + e.getMessage());
			System.out.println("Error::" + e.getMessage());
			throw new ReadFileException("Error read file::" + e.getMessage());
		}
		return data;
	}

}
