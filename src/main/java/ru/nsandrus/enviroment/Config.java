package ru.nsandrus.enviroment;

import static ru.nsandrus.entity.KeyWords.SCENARIO;
import static ru.nsandrus.entity.KeyWords.SCENARIOFOR;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.log4j.Logger;

import ru.nsandrus.avs.Scenario;
import ru.nsandrus.entity.DbEntity;
import ru.nsandrus.entity.DomainGf;
import ru.nsandrus.entity.MQueueEntity;

public class Config {

	private static final String QUEUE_NAME = "QUEUENAME=";
	private static final String PASSWORD = "PASSWORD=";
	private static final String USER_ID = "USERID=";
	private static final String CHANNEL_NAME = "CHANNELNAME=";
	private static final String QUEUE_MANAGER_NAME = "QUEUEMANAGERNAME=";
	private static final String PORT = "PORT=";
	private static final String HOST_NAME = "HOSTNAME=";

	private static final String DB_PASSWORD = "DB_PASSWORD=";
	private static final String DB_USER = "DB_USER=";
	private static final String DB_CONNECTION = "DB_CONNECTION=";
	private static final String DB_DRIVER = "DB_DRIVER=";

	private static final String DATABASE = "DATABASE";
	private static final String DOMAINGF = "DOMAINGF";
	private static final String URL = "URL=";
	private static final String LOGIN = "LOGIN=";

	private static final int LEN_WORD_PLAN = 5;
	private static final String PAGE_LOAD_TIMEOUT = "PAGE_LOAD_TIMEOUT=";
	private static final String IMPLICITLY_WAIT = "IMPLICITLY_WAIT=";

	static Logger LOG = Logger.getLogger(Config.class);
	static HashMap<String, String> vars;

	public static ArrayList<String> getArrayPlan(String data) {
		String[] lines = data.split("\n");
		ArrayList<String> datas = new ArrayList<String>();
		for (String line : lines) {
			datas.add(line);
		}
		datas.add("");
		return datas;
	}

	/*
	 * DB_DRIVER = "oracle.jdbc.driver.OracleDriver" DB_CONNECTION = "jdbc:oracle:thin:@localhost:1521:MKYONG" DB_USER = "user" DB_PASSWORD = "password"
	 */
	public static List<DbEntity> getDataBases(String fileConfigText) {
		List<DbEntity> databases = null;
		String[] lines = fileConfigText.split("\n");
		boolean flagDB = false;
		String dbDriver = "";
		String dbConnection = "";
		String dbUser = "";
		String dbPassword = "";
		String dataBaseNameList = "";

		for (String line : lines) {
			String lineTrim = line.trim();
			if (isStartWith(lineTrim, DATABASE) && !flagDB) {
				if (databases == null) {
					databases = new ArrayList<>();
					LOG.debug("create new List databases");
				}
				flagDB = true;
				dataBaseNameList = lineTrim.substring(DATABASE.length()).trim();
				LOG.debug("Name of base [" + dataBaseNameList + "]");
				continue;
			}

			if (flagDB) {

				if (isStartWith(lineTrim, DB_DRIVER)) {
					dbDriver = lineTrim.substring(DB_DRIVER.length(), lineTrim.length());
				} else if (isStartWith(lineTrim, DB_CONNECTION)) {
					dbConnection = lineTrim.substring(DB_CONNECTION.length(), lineTrim.length());
				} else if (isStartWith(lineTrim, DB_USER)) {
					dbUser = lineTrim.substring(DB_USER.length(), lineTrim.length());
				} else if (isStartWith(lineTrim, DB_PASSWORD)) {
					dbPassword = lineTrim.substring(DB_PASSWORD.length(), lineTrim.length());

				}
			}

			if (flagDB && lineTrim.isEmpty()) {
				flagDB = false;
				DbEntity db = new DbEntity();
				db.dbDriver = dbDriver;
				db.dbConnection = dbConnection;
				db.dbUser = dbUser;
				db.dbPassword = dbPassword;
				db.dataBaseNameList = dataBaseNameList;
				LOG.debug("insert new data in List");
				databases.add(db);

			}
		}
		LOG.debug("Databases=" + databases);
		return databases;
	}

	public static List<MQueueEntity> getQueues(String fileConfigText) {
		List<MQueueEntity> queues = null;
		String[] lines = fileConfigText.split("\n");
		boolean flagJms = false;
		String queueNameList = "";
		int portJms = 1450;

		String hostName = "";
		String queueManager = "";
		String channel = "";
		String user = "";
		String password = "";
		String queueName = "";
		Hashtable<String, String> props = new Hashtable<String, String>();

		for (String line : lines) {
			String lineTrim = line.trim();
			if (isStartWith(lineTrim, "JMS") && !flagJms) {
				if (queues == null) {
					queues = new ArrayList<>();
				}
				flagJms = true;
				queueNameList = lineTrim.substring(4);
				continue;
			}
			if (flagJms) {
				if (isStartWith(lineTrim, PORT)) {
					LOG.debug(PORT + lineTrim.substring(5));
					portJms = Integer.parseInt(lineTrim.substring(5));
				} else if (isStartWith(lineTrim, HOST_NAME)) {
					hostName = lineTrim.substring(HOST_NAME.length(), lineTrim.length());
				} else if (isStartWith(lineTrim, QUEUE_MANAGER_NAME)) {
					queueManager = lineTrim.substring(QUEUE_MANAGER_NAME.length(), lineTrim.length());
				} else if (isStartWith(lineTrim, CHANNEL_NAME)) {
					channel = lineTrim.substring(CHANNEL_NAME.length(), lineTrim.length());
				} else if (isStartWith(lineTrim, USER_ID)) {
					user = lineTrim.substring(USER_ID.length(), lineTrim.length());
				} else if (isStartWith(lineTrim, PASSWORD)) {
					password = lineTrim.substring(PASSWORD.length(), lineTrim.length());
				} else if (isStartWith(lineTrim, QUEUE_NAME)) {
					queueName = lineTrim.substring(QUEUE_NAME.length(), lineTrim.length());
				} else { // props
					LOG.debug("prop=" + lineTrim);
					String[] prop = lineTrim.split("::");
					if (prop.length == 2) {
						LOG.debug("prop[0]=" + prop[0] + "::" + prop[1]);
						props.put(prop[0], prop[1]);
					}
				}
			}

			if (flagJms && lineTrim.isEmpty()) {
				flagJms = false;
				MQueueEntity queue = new MQueueEntity();
				queue.channel = channel;
				queue.hostName = hostName;
				queue.password = password;
				queue.port = portJms;
				queue.queueManager = queueManager;
				queue.queueName = queueName;
				queue.user = user;
				queue.queueNameList = queueNameList;
				queue.props = props;
				queue.message = "";
				queues.add(queue);
				props = new Hashtable<String, String>();
			}
		}
		LOG.debug("Queue=" + queues);
		return queues;
	}

	public static List<Scenario> getScenaries(String codeAvs) {
		LOG.debug("START::create array scenaries");
		List<Scenario> scenaries = new ArrayList<>();
		ArrayList<String> datas = Config.getArrayPlan(codeAvs);

		Marker marker = Marker.EMPTY;
		Scenario sc = null;
		for (int i = 0; i < datas.size(); i++) {
			String line = datas.get(i).trim();

			if (line.trim().isEmpty()) {

				if (marker.equals(Marker.SCENARIO)) {
					scenaries.add(sc);
				}
				marker = Marker.EMPTY;
			}

			if (line.toUpperCase().trim().startsWith(SCENARIO + " ") && marker.equals(Marker.EMPTY)) {
				marker = Marker.SCENARIO;
				sc = new Scenario();
				sc.name = line.trim().substring(9);
				continue;
			}

			if (line.toUpperCase().trim().startsWith(SCENARIOFOR + " ") && marker.equals(Marker.EMPTY)) {
				marker = Marker.SCENARIO;
				sc = new Scenario();
				sc.name = line.trim().substring(12);
				sc.isScenarioFor = true;
				continue;
			}

			if (marker.equals(Marker.SCENARIO)) {
				sc.code.append(line + "\n");
			}
		}
		return scenaries;
	}

	public static String getPlanName(String codeAvs) {
		String result = "";
		try {
			Pattern p = Pattern.compile("^plan .+$", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE | Pattern.MULTILINE);
			Matcher m = p.matcher(codeAvs);
			boolean resErr = m.find();
			if (resErr) {
				int start = m.start() + LEN_WORD_PLAN;
				int end = m.end();
				result = codeAvs.substring(start, end);
			}
		} catch (PatternSyntaxException e) {
			result = e.getMessage();
		}
		return result;
	}

	public static HashMap<String, String> getVars(String fileConfigText) {
		String[] linesCfgFile = fileConfigText.split("\n");
		LOG.debug("VARS=" + Arrays.toString(linesCfgFile));
		for (String lineCfg : linesCfgFile) {
			String lineTrim = lineCfg.trim();
			if (isStartWith(lineTrim, "VAR")) {
				addVars(lineTrim);
				continue;
			}
		}
		if (vars != null) {
			LOG.debug("Vars" + vars.toString());
		}
		return vars;
	}

	private static void addVars(String lineTrim) {
		if (vars == null) {
			vars = new HashMap<>();
		}
		String varsElement = lineTrim.substring(4);
		String[] kv = varsElement.split("=");
		String nameVars = kv[0];
		String valueVars = kv[1];
		vars.put(nameVars, valueVars);
		LOG.debug("found var " + nameVars + "==" + valueVars);
	}

	public static boolean isStartWith(String lineTrim, String command) {
		return lineTrim.toUpperCase().startsWith(command);
	}

	public static String getMainPath(String fileConfigText) {
		String[] lines = fileConfigText.split("\n");
		for (String line : lines) {
			if (line.trim().startsWith("PATH=")) {
				String path = line.trim().substring(5);
				LOG.debug("Path=" + path);
				if (!path.endsWith("/")) {
					path += "/";
				}
				LOG.info("Main PATH=" + path);
				return path;
			}
		}
		return "";
	}

	public static List<DomainGf> getDomainsGf(String fileConfigText) {

		List<DomainGf> servers = null;
		String[] lines = fileConfigText.split("\n");
		boolean flagDomain = false;
		String url = "";
		String login = "";
		String password = "";
		String name = "";
		int pageLoadTimeout = 2;
		int implicitlyWait = 10;

		for (String line : lines) {
			String lineTrim = line.trim();
			if (isStartWith(lineTrim, DOMAINGF) && !flagDomain) {
				if (servers == null) {
					servers = new ArrayList<>();
					LOG.debug("create new List servers");
				}
				flagDomain = true;
				name = lineTrim.substring(DOMAINGF.length()).trim();
				continue;
			}

			if (flagDomain) {

				if (isStartWith(lineTrim, URL)) {
					url = lineTrim.substring(URL.length(), lineTrim.length());
				} else if (isStartWith(lineTrim, LOGIN)) {
					login = lineTrim.substring(LOGIN.length(), lineTrim.length());
				} else if (isStartWith(lineTrim, PASSWORD)) {
					password = lineTrim.substring(PASSWORD.length(), lineTrim.length());
				} else if (isStartWith(lineTrim, PAGE_LOAD_TIMEOUT)) {
					String number = lineTrim.substring(PAGE_LOAD_TIMEOUT.length(), lineTrim.length());
//					System.out.println("number=" + number);
					pageLoadTimeout = Integer.parseInt(number);
				} else if (isStartWith(lineTrim, IMPLICITLY_WAIT)) {
					implicitlyWait = Integer.parseInt(lineTrim.substring(IMPLICITLY_WAIT.length(), lineTrim.length()));
				}
			}

			if (flagDomain && lineTrim.isEmpty()) {
				flagDomain = false;
				DomainGf server = new DomainGf();
				server.login = login;
				server.password = password;
				server.url = url;
				server.name = name;
				server.pageLoadTimeout = pageLoadTimeout;
				server.implicitlyWai = implicitlyWait;
				LOG.debug("insert new data in List");
				servers.add(server);

			}
		}
		LOG.debug("Servers=" + servers);
		return servers;
	}
}
