package ru.nsandrus.report;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import ru.nsandrus.avs.PlanAvs;
import ru.nsandrus.entity.AssertFailed;
import ru.nsandrus.entity.LogString;

public class Report {
	
	Logger LOG = Logger.getLogger(Report.class);
	List<LogString> history = new ArrayList<>();
	private String formatOutput = "\t\tScenaries=%-5d\tPassed=%-5d\tFailed=%-5d\n";

	public void вывести_Отчет(PlanAvs plan) {
		try {
			PrintWriter writer = new PrintWriter(new OutputStreamWriter(System.out, "CP866"));
			outTable(plan, writer);
		} catch (UnsupportedEncodingException e) {
			System.out.println("error encoded report output." + e.getMessage());
		}
		
	};

	public void вывести_Отчет_Файл(PlanAvs план) throws UnsupportedEncodingException {
		File file = createFile(план, "txt");
		PrintWriter out = null;
		try {
			out = new PrintWriter(file.getAbsoluteFile(),"UTF-8");
		} catch (FileNotFoundException e) {
			System.out.println("Ошибка записи отчета." + e.getMessage());
		} finally {
			outTable(план, out);
			out.close();
		}
	}

	public void вывести_Отчет_Файл_HTML(PlanAvs план) throws UnsupportedEncodingException {
		File file = createFile(план,"html");
		PrintWriter out = null;
		try {
			out = new PrintWriter(file.getAbsoluteFile(),"UTF-8");
		} catch (FileNotFoundException e) {
			System.out.println("Ошибка записи отчета." + e.getMessage());
		} finally {
			outTable_HTML(план, out);
			out.close();
		}
	}

	private void outTable_HTML(PlanAvs plan, PrintWriter out) {
		out.println("<head><script type=\"text/javascript\">function toggle_show(id) {"
				+ "document.getElementById(id).style.display = document.getElementById(id).style.display == 'none' ? 'block' : 'none';}</script></head>");
		out.println("<body>");
		out.print("<h3>ТестПлан - [" + plan.name + "], время работы [" + plan.workedTime + "] секунд</h3><br/>");
		String formatOutHtml = "<pre>" + formatOutput;
		out.printf(formatOutHtml, plan.countScenario, plan.countPassed, plan.countFailed);
		out.println(
				"<br/>==============================================================================================\n");
		for (LogString str : history) {
			String resultPass = "<font color='green'>" + str.assertsPassed.toString() + "</font>";
			out.printf("<br/>[%-5d Name=%-160s] Time=%-5d ", str.id, str.scenarioName, str.timeWork);

			if (str.assertsPassed.toString().length() > 2) {
				out.printf("Passed=%-40s", resultPass);
			}
			if (str.assertsFailed.toString().length() > 2) {
				out.printf("<br/><font color='red'><div onClick=\"toggle_show('" + str.id
						+ "')\">\t<b>Нажми чтобы просмотреть описание ошибки</b></div><div id=\"" + str.id
						+ "\" style=\"display: none\">");
				for (AssertFailed failed : str.assertsFailed) {
					out.print("\n\t" + failed.typeAssert);
					out.print("\n\t" + failed.errorMessage);
					String response = failed.errorResponse;
					out.print("\n\t" + response);
				}
				out.printf("</div></font>");
			}
		}
		out.println("</pre></body>");
		out.flush();
	}
	
	private void outTable(PlanAvs plan, PrintWriter out) {
		out.println("\n\n==================================  О Т Ч Е Т  ===============================================");
		out.print("ТестПлан - [" + plan.name + "], время работы [" + plan.workedTime + "] секунд\n\n");
		out.printf(formatOutput, plan.countScenario, plan.countPassed,plan.countFailed);
		out.println("==============================================================================================\n");		
		
		String resultPass = "";
		for (LogString str :history) {
				resultPass = str.assertsPassed.toString();
			out.printf("\n[%-5d Name=%-80s] Time=%-5d", str.id, str.scenarioName, str.timeWork);
			if (str.assertsPassed.toString().length() > 2) { 
				out.print("Passed=" + resultPass);	
			}
			if (str.assertsFailed.toString().length() > 2) {
				
				for ( AssertFailed failed : str.assertsFailed) {
					out.print("\n\t" + failed.typeAssert);
					out.print("\n\t" + failed.errorMessage);
					String response = failed.errorResponse.length() > 1024 ? failed.errorResponse.substring(0, 1024) : failed.errorResponse;
					out.print("\n\t" + response);
				}
			}

		}

		out.flush();
	}

	protected File createFile(PlanAvs план, String ext) {
		Date curDate = new Date();
		SimpleDateFormat format = new SimpleDateFormat();
		format = new SimpleDateFormat("yyyy_MM_dd-HH_mm_ss");
		String DateToStr = format.format(curDate);

		File dir = new File("Reports");
		if (!dir.exists()) {
			dir.mkdir();
		}

		File file = new File("Reports/" + план.name + "_" + DateToStr + "." + ext);
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				System.out.println("Ошибка создания отчета." + e.getMessage());
			}
		}
		return file;
	}


	public void addReportFatal(String message, int countScenario) {
		System.out.println("\tFATAL::" + message);
		LogString event = getEvent(countScenario);
		event.startTime = System.currentTimeMillis();
		AssertFailed assertFailed = new AssertFailed();
		assertFailed.errorMessage = message;
		assertFailed.errorResponse = "";
		assertFailed.typeAssert = "FATAL";
		event.assertsFailed.add(assertFailed);
		updateEvent(countScenario, event);
	}

	public void addBeginScenario(long currentTimeMillis, int countScenario, String trim) {
		LogString event = new LogString();
		event.startTime = currentTimeMillis;
		event.id = countScenario;
		event.scenarioName = trim;
		history.add(event);
	}

	public void addReporFailed(int countScenario, String typeAssert, String errorMessage, String errorResponse) {
		LogString event = getEvent(countScenario);
		event.startTime = System.currentTimeMillis();
		AssertFailed assertFailed = new AssertFailed();
		assertFailed.errorMessage = errorMessage;
		assertFailed.errorResponse = errorResponse;
		assertFailed.typeAssert = typeAssert;
		event.assertsFailed.add(assertFailed);
		updateEvent(countScenario, event);
		System.out.println("\n\tFAILED |" + typeAssert + "| " + errorMessage);
	}

	private LogString getEvent(int countScenario) {
		for (LogString log:history) {
			if (log.id == countScenario) {
				return log;
			}
		}
		return new LogString();
	}

	public void addReportPassed(int countScenario, String typeAssert) {
		LogString event = getEvent(countScenario);
		event.assertsPassed.add(typeAssert);
		updateEvent(countScenario, event);
	}

	private void updateEvent(int countScenario, LogString event) {
		for (LogString log:history) {
			if (log.id == countScenario) {
				log = event;
				return;
			}
		}
	}

	public void addReporFailedScenario(int countScenario, String exceptionMessage) {
		LogString event = getEvent(countScenario);
		event.scenarioException = exceptionMessage;
		updateEvent(countScenario, event);
		
	}

	public void addEndScenario(int countScenario, int workedTime) {
		LogString event = getEvent(countScenario);
		event.timeWork = workedTime;
		updateEvent(countScenario, event);
		
	};
}
