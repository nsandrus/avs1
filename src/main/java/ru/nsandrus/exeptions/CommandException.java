package ru.nsandrus.exeptions;

public class CommandException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2816180331229340125L;
	
	String textException = "";

	public CommandException(String string) {
		textException = string;
	}

	 public String getMessage() {
	        return textException;
	    }
}
