package ru.nsandrus.exeptions;

public class ScenarioException extends Exception {

	private String textException="";
	/**
	 * 
	 */
	private static final long serialVersionUID = -8297248170732533496L;

	public ScenarioException(String string) {
		textException = string;
	}

	 public String getMessage() {
	        return textException;
	    }
}
