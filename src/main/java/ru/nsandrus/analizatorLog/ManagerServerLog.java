package ru.nsandrus.analizatorLog;

import org.apache.log4j.Logger;

public class ManagerServerLog {

	Logger LOG = Logger.getLogger(ManagerServerLog.class);
	private String serverLog;
	private AnalizatorFMQ fmq;

	public void startServerLog() {
		LOG.debug("Start");
		if (serverLog.isEmpty()) {
			return;
		}

		String имя_Файла_Логов = serverLog;
		fmq = new AnalizatorFMQ();
		fmq.запустить_Поток_Смотрящего_За_Файлом(имя_Файла_Логов);
		sleep(4);
		LOG.debug("End");
	}

	private void sleep(int i) {
		LOG.info("Sleep in " + i);
		try {
			Thread.sleep(i * 1000);
		} catch (InterruptedException e) {
		}
	}

	public void setServerLog(String result) {
		serverLog = result;
	}

	public void stopServerLog() {
		if (serverLog.isEmpty()) {
			return;
		}
		fmq.закончить_Наблюдение();
	}

	public void pause() {
		if (fmq != null) {
			fmq.приостановить_Наблюдение();
		}
	}

	public boolean isEmpty() {
		return serverLog.isEmpty();
	}

	public String getData() {
		return fmq.получить_Текст();
	}

	public void startWatchLogFile() {
		if (fmq != null) {
			fmq.начать_Наблюдение("");
			fmq.clear();
		}

	}

}
