package ru.nsandrus.analizatorLog;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;

import org.apache.log4j.Logger;

public class LoggerGF implements ReadLogger {

	private static final int BUFFER_SIZE = 4096;
	// need charset UTF8 or CP1251
	private static final String END_GF_LINE = "#]";
	private static final String BEGIN_GF_LINE = "[#";
	private static final int МАКСИМАЛЬНЫЙ_РАЗМЕР_СООБЩЕНИЯ = 1_000_000;
	final static Logger LOG = Logger.getLogger(LoggerGFTest.class);
	private long размер_Файла_Давно;
	private String имя_Файла;
	private RandomAccessFile fileCursor;
	private StringBuffer bufferMessage = new StringBuffer(МАКСИМАЛЬНЫЙ_РАЗМЕР_СООБЩЕНИЯ);

	public LoggerGF(String filenameIn) throws FileNotFoundException {
		this.имя_Файла = filenameIn;
		fileCursor = new RandomAccessFile(имя_Файла, "r");
	}

	public LoggerGF(String filenameIn, String charsetName) throws FileNotFoundException, UnsupportedEncodingException {
		this.имя_Файла = filenameIn;
		fileCursor = new RandomAccessFile(имя_Файла, "r");
	}

	@Override
	public String getMessage() throws IOException {
		// LOG.trace("ЧИТАЕМ СООБЩЕНИЕ::POS=" + fileCursor.getFilePointer() + "::LEN=" + fileCursor.length());
		String returnMessage = "";
		
		

		int beginIndex = bufferMessage.indexOf(BEGIN_GF_LINE);
		int endIndex = bufferMessage.indexOf(END_GF_LINE);
		LOG.trace("begin=" + beginIndex + " end=" + endIndex);
		
		if (beginIndex < 0 && endIndex <0) {
			getGlassFishMessage();
			beginIndex = bufferMessage.indexOf(BEGIN_GF_LINE);
			endIndex = bufferMessage.indexOf(END_GF_LINE);
			LOG.trace("read and begin=" + beginIndex + " end=" + endIndex);
		} else	if (beginIndex < 0 ) {
			bufferMessage.setLength(0);
			LOG.trace("set length = 0");
		}  
			
		
		
		if (beginIndex >= 0 && endIndex > beginIndex) {
			returnMessage = bufferMessage.substring(beginIndex, endIndex + 2);
			bufferMessage.replace(beginIndex, endIndex + 2, "");
			LOG.trace("replace begin=" + beginIndex + " end=" + endIndex);
			return returnMessage;
		}
		
		if (beginIndex >= 0 && endIndex <= 0) {
			getGlassFishMessage();
			
			}
		
		
		
		beginIndex = bufferMessage.indexOf(BEGIN_GF_LINE);
		endIndex = bufferMessage.indexOf(END_GF_LINE);
		if (beginIndex > endIndex) {
			LOG.trace("delete not completed message");
			bufferMessage.replace(0, endIndex + 2, "");
			beginIndex = bufferMessage.indexOf(BEGIN_GF_LINE);
			endIndex = bufferMessage.indexOf(END_GF_LINE);
		}

		
		LOG.trace("BUILD begin=" + beginIndex + " end=" + endIndex);
		if (beginIndex >= 0 && endIndex > beginIndex) {
			returnMessage = bufferMessage.substring(beginIndex, endIndex + 2);
			bufferMessage.replace(beginIndex, endIndex + 2, "");
		}
		
		LOG.trace("СООБЩЕНИЕ::" + returnMessage);
		return returnMessage;
	}

	private String readMessageByByte(String charset) throws EOFException {
		String сообщение = null;
		try {
			byte[] b = new byte[BUFFER_SIZE];
			fileCursor.read(b);
			сообщение = new String(b, charset);
			LOG.trace("read "+BUFFER_SIZE+" byte to buffer=" + сообщение);
			// s.getBytes(Charset.forName("utf-8"))) {
		} catch (IOException e) {
			// LOG.error("POSITION="+ fileCursor.getFilePointer() + "::LENGTH="+fileCursor.length()+"::ERROR READ::" + e.getMessage() + "::СЧИТАЛИ=" + сообщение);
			throw new EOFException("Конец файла");
		}
		return сообщение;
	}

		
	private void getGlassFishMessage() throws IOException {
		
		checkRollingFile();
		LOG.trace("ЧИТАЕМ GF СООБЩЕНИЕ");
		while (fileCursor.length() > fileCursor.getFilePointer()) {
			bufferMessage.append(readMessageByByte("UTF-8"));
			if (bufferMessage.indexOf(END_GF_LINE) >= 0) {
				break;
			}
			if (bufferMessage.length() >= МАКСИМАЛЬНЫЙ_РАЗМЕР_СООБЩЕНИЯ) {
				System.out.println("MAX length");
				break;
			}
		}
	}
	
	@Override
	public boolean hasNext() throws IOException {
		boolean правда_Готов_Читать = false;
		if (fileCursor.length() > fileCursor.getFilePointer()) {
			правда_Готов_Читать = true;
			LOG.trace("ГОТОВ ЧИТАТЬ::POSITION=" + fileCursor.getFilePointer() + "::LEN=" + fileCursor.length());
		}

		if (!правда_Готов_Читать) {
			checkRollingFile();
		}
		return правда_Готов_Читать;
	}

	protected void checkRollingFile() throws IOException, FileNotFoundException {
		long размер_Файла_Сейчас = fileCursor.length();
		if (размер_Файла_Сейчас < размер_Файла_Давно) {
			System.out.println("Rolling file");
			sleep();
			long размер_Файла_Прямо_Сейчас = fileCursor.length();
			if (размер_Файла_Прямо_Сейчас < размер_Файла_Давно) {
				fileCursor.close();
				fileCursor = new RandomAccessFile(имя_Файла, "r");
			}
		}
		размер_Файла_Давно = размер_Файла_Сейчас;
	}

	protected void sleep() {
		try {
			LOG.trace("SLEEP 1.4");
			Thread.sleep(1400);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void close() throws IOException {
		fileCursor.close();
	}

	@Override
	public void goToEndFile() {
		LOG.trace("GO TO END OF FILE");
		try {
			fileCursor.seek(fileCursor.length());
			LOG.trace("position in file " + fileCursor.getFilePointer());
		} catch (IOException e) {
			LOG.debug("error seek ent of file");
		}
		LOG.trace("Find the end of file");
	}

}
