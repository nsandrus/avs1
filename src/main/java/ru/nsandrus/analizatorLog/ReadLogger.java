package ru.nsandrus.analizatorLog;

import java.io.IOException;

public interface ReadLogger {

		public String getMessage() throws IOException;
		boolean hasNext()throws IOException;
		public void close() throws IOException;
		public void goToEndFile();

}
