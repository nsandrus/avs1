package ru.nsandrus.analizatorLog;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.log4j.Logger;


/**
 * Created by AVSurkov on 10/02/2016.
 */
public class Смотрящий_За_Файлом implements Runnable {

	final static Logger LOG = Logger.getLogger(Смотрящий_За_Файлом.class);

	private String имяФайла;
	private volatile String текст_Фильтра = "";
	private volatile String строка_С_Ошибкой = "";
	private String кодировка = "CP1251";
	StringBuilder result;
	enum stateOfWork {START, RESUME, SUSPEND, STOP};
	private volatile stateOfWork статус_Работы = stateOfWork.STOP;


	public Смотрящий_За_Файлом(String fileName) {
		LOG.info("Смотрящий за файлом=" + fileName);
		this.имяФайла = fileName;
		result = new StringBuilder();
	}

	public void run() {
		LOG.info("Запущен поток для чтения файла =" + имяФайла);
		статус_Работы = stateOfWork.START;
		строка_С_Ошибкой = "";

		читаем_Файл_В_Цикле();

		LOG.info("Конец");
	}


	private void читаем_Файл_В_Цикле() {
		LOG.info("Состояние работы = " + статус_Работы);
			ReadLogger readerGf = getInstanceReader();
			readerGf.goToEndFile();

			while (статус_Работы != stateOfWork.STOP) {
				try {
					if (readerGf.hasNext()) {
						String строка_Сообщения_Лога = readerGf.getMessage();
						if (статус_Работы == stateOfWork.START) {
							result.append(строка_Сообщения_Лога);
							result.append(System.getProperty("line.separator"));
						}
					} else {
						sleep();
					}
				} catch (IOException e) {
//					e.printStackTrace();
				}
			}

	LOG.trace("Конец::результат=" + result);
	}

	public void приостановиться() {
		статус_Работы = stateOfWork.SUSPEND;
		String данные_Для_Фильтра = result.toString();
		Фильтр фильтр = new Фильтр(данные_Для_Фильтра, текст_Фильтра);
		строка_С_Ошибкой = фильтр.получить_Текст_Ошибки();
	}


	public String получить_Текст_Ошибки() {
		return строка_С_Ошибкой;
	}
	public String получить_Текст() {
		return result.toString();
	}
	public void очистка(String analize) {
		this.result.setLength(0);
	}

	private ReadLogger getInstanceReader() {
		ReadLogger readerGf = null;
		try {
			readerGf = new LoggerGF(имяФайла, кодировка);
		} catch (FileNotFoundException | UnsupportedEncodingException e2) {
			e2.printStackTrace();
		}
		return readerGf;
	}

	private void sleep() {
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
		}
	}

	public void начать_Наблюдение(String фильтр) {  // фильтр по идеи сюда не надо
		статус_Работы = stateOfWork.START;
		текст_Фильтра = фильтр;
		строка_С_Ошибкой = "";
		result.setLength(0);

	}

	public void закончить() {
		статус_Работы = stateOfWork.STOP;
	}

}
