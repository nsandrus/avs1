package ru.nsandrus.mock;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

import org.apache.log4j.Logger;

public class SocketProcessor implements Runnable {
	private static final String HTTP_500 = "HTTP/1.1 500 Internal Server Error\n\nNot found XML= for this Request in MOCK on path=";
	private static final String KEY_METHOD = "METHOD=";
	private static final String KEY_XML = "XML=";
	private static final String KEY_URL = "URL=";
	private Socket сокет;
	private InputStream поток_Чтения;
	private OutputStream поток_Записи;
	private Mock заглушка;
	private String inPath = "";
	final static Logger LOG = Logger.getLogger(SocketProcessor.class);

	public SocketProcessor(Socket socket, Mock заглушка) {
		this.сокет = socket;
		this.заглушка = заглушка;
		try {
			this.поток_Чтения = сокет.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			this.поток_Записи = сокет.getOutputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		String тело_Запроса = чтение_Тела_Запроса();
		LOG.debug("запрос был на порт " + заглушка.port + " BODY=" + тело_Запроса);
		String новыйОтвет = получить_Настроенный_Ответ(тело_Запроса);
		послать_Ответ(новыйОтвет);
		try {
			поток_Записи.close();
			поток_Чтения.close();
			сокет.close();
		} catch (Throwable t) {
			/* do nothing */
		}
	}

	protected void заснуть() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	
	private String чтение_Тела_Запроса() {
		StringBuilder весь_Запрос = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(поток_Чтения, "UTF-8"));
			waitInputData(in);
			while (in.ready()) {
				char ch = (char) in.read();
				весь_Запрос.append(ch);
			}
		} catch (IOException | NullPointerException e) {
			e.printStackTrace();
		}

		String[] paths = весь_Запрос.toString().split(" ");
		if (paths.length > 2) {
			inPath = paths[1].trim();
			LOG.debug("Incoming path url=" + inPath.trim());
		}

		// заглушка может отвечать с пустой строки. тогда массива будет 3
		LOG.trace("all incoming request::" + весь_Запрос.toString());
		int start = весь_Запрос.indexOf("\n\r");
		if (start < 0) {
			LOG.debug("Позиция пустой строки=" + start + " ::длина запроса=" + весь_Запрос.length());
			return "";
		}
		LOG.debug("Index start=" + start);
		String body = весь_Запрос.substring(start + 1);
		LOG.debug("Body=" + body);
		return body;
	}

	
	protected void waitInputData(BufferedReader in) {
		/*
		 * блок чтобы гарантированно считать тело запроса
		 */
		try {
			while (!in.ready()) {
				try {
					Thread.sleep(30);
				} catch (InterruptedException e1) {
				}
			}
		} catch (IOException e1) {
		}
	}

	private void послать_Ответ(String ответ) {
		LOG.debug("MOCK RESPONSED=" + ответ);
		try {
			поток_Записи.write(ответ.getBytes("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			поток_Записи.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * тут надо обернуть в HTPP 200 OK к примеру или это прямо указывать в
	 * заглушке
	 */
	private String получить_Настроенный_Ответ(String входящийЗапрос) {
		заглушка.receive = входящийЗапрос;
		LOG.trace("incoming raw URL=[" + inPath + "]");
		inPath = getWithoutParam(inPath);
		if (isFoundXmlResponse(входящийЗапрос)) {
			return заглушка.response;
		}
		String mes = HTTP_500 + inPath;
		System.out.println(mes);
		return mes;
	}

	private boolean isFoundXmlResponse(String request) {

		String[] blocks = заглушка.responseAll.split(KEY_URL);

		for (String block : blocks) {
			LOG.debug("found blocks response [" + block + "]");
			if (block.trim().isEmpty()) {
				continue;
			}

			int end = block.indexOf("\n");
			String method = getMethod(block);
			String url = block.substring(0, end).trim();

			if (inPath.equalsIgnoreCase(url)) {
				if (method.isEmpty() || request.contains(method)) {
					заглушка.response = block.substring(block.indexOf(KEY_XML) + 4);
					return true;
				}
			}
		}
		return false;
	}

	private String getMethod(String block) {
		String[] lines = block.split("\n");
		for (String line : lines) {
			if (line.trim().toUpperCase().startsWith(KEY_METHOD)) {
				return line.trim().substring(7);
			}
		}
		return "";
	}

	private String getWithoutParam(String inPath2) {
		int index = inPath2.indexOf("?");
		if (index > 0) {
			return inPath2.substring(0, index);
		}
		return inPath2;
	}

	protected void logError(String mes) {
		System.out.println(mes);
		LOG.error(mes);
	}
}
