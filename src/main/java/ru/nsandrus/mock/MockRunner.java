package ru.nsandrus.mock;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.log4j.Logger;

public class MockRunner extends Thread {

	final static Logger LOG = Logger.getLogger(MockRunner.class);

	private final static long МАКСИМАЛЬНАЯ_ПРОДОЛЖИТЕЛЬНОСТЬ_ЖИЗНИ_ЗАГЛУШКИ = 6000;
	private volatile boolean isAlive;
	private volatile Mock globalMock;
	private volatile int status = 0;

	public String getNameMock() {
		return globalMock.name;
	}

	public MockRunner(Mock mock) {
		globalMock = mock;
	}

	public Thread завершить_Работу() {
		isAlive = false;
		return this;
	}

	public void run() {
		status = 1;
		// тут надо считать запрос из файла или ранее записывать в заглушку овтет

		LOG.info("Начало работы заглушки " + globalMock.name + " на порту " + globalMock.port + " с ответом=" + globalMock.response);
		isAlive = true;
		Long текущее_Время = System.currentTimeMillis();
		Long время_Завершения_Работы = текущее_Время + (МАКСИМАЛЬНАЯ_ПРОДОЛЖИТЕЛЬНОСТЬ_ЖИЗНИ_ЗАГЛУШКИ * 1000L);

		ServerSocket ss;
		try {
			ss = new ServerSocket(globalMock.port);
			ss.setSoTimeout(500);
		} catch (IOException e) {
			LOG.error("Error start Mock on port="+globalMock.port+"::name=" + globalMock.name + "::" + e.getMessage());
			System.out.println("Error start Mock on port="+globalMock.port+"::name=" + globalMock.name + "::" + e.getMessage());
			return;
		}
		while (isAlive && текущее_Время <= время_Завершения_Работы) {
			try {
				Socket socket = null;
				socket = ss.accept();

				LOG.debug("Получили запрос, обрабатываем в другом потоке его");
				new Thread(new SocketProcessor(socket, globalMock)).start();
				текущее_Время = System.currentTimeMillis();
			} catch (Throwable e1) {
				текущее_Время = System.currentTimeMillis();
			}
		}
		try {
			ss.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		status = 2;
	}

	public Mock получить_Данные() {
		if (status == 2) {
			return globalMock;
		} else {
			return null;
		}
	}
}
