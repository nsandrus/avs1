package ru.nsandrus.mock;

import org.apache.log4j.Logger;

import ru.nsandrus.file.FileReader;
import ru.nsandrus.file.ReadFileException;

public class Mock {

	Logger LOG = Logger.getLogger(Mock.class);

	public int port;
	public String name;
	public String fileResponse = "";
	public String response = "";
	public String responseAll = "";
	public String receive = "";

	public Mock(String nameMock, int port2, String fileResponse) {
		this.name = nameMock;
		this.port = port2;
		this.fileResponse = fileResponse;
	}

	public void readResponseFromFile(String mainPath) throws ReadFileException  {
			String textFile = FileReader.readFile(mainPath + fileResponse);
			responseAll = textFile.trim();
			setResponseMessage(responseAll);
	}

	private void setResponseMessage(String data) {
		LOG.debug("Mock::Incoming data=" + data);
		int start = data.indexOf("XML=");
		if (start <0) {
			String textError = "NOT FOUND SOAP MESSAGE::data=" + data;
			LOG.error(textError);
			response = textError;
			System.out.println(textError);
			return;
		}
		// может быть несколько овтетов в зависимости от URL входящего
		int endXml = data.indexOf("\nURL=", start+4);
		if (endXml < 0) {
		response = data.substring(start + 4);
		} else {
			response = data.substring(start + 4, endXml - 1);
		}
	}

	public void setResponse(String response2) {
		responseAll = response2.trim();
		setResponseMessage(responseAll);
	}

	@Override
	public String toString() {
		return "Mock [name=" + name + ", port=" + port + ", response=" + fileResponse + "]";
	}
}
