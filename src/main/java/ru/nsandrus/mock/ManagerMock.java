package ru.nsandrus.mock;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ru.nsandrus.enviroment.Config;
import ru.nsandrus.file.ReadFileException;

public class ManagerMock {

	Logger LOG = Logger.getLogger(ManagerMock.class);
	
	List<Mock> mocks;
	List<MockRunner> listMocksRunner;
	List<Thread> threads;
	
	public void startMocks(String mainPath) throws ReadFileException {
		LOG.info("Start");
		if (mocks == null) {
			return;
		}
		listMocksRunner = new ArrayList<MockRunner>();
		threads = new ArrayList<>();

		for (Mock tmpMock : mocks) {
			tmpMock.readResponseFromFile(mainPath);
			LOG.info("Start mock on port=" + tmpMock.port);
			MockRunner обработчик_Заглушки = new MockRunner(tmpMock);
			Thread thread = new Thread(обработчик_Заглушки);
			thread.start();
			threads.add(thread);
			listMocksRunner.add(обработчик_Заглушки);
		}
		LOG.info("End");
	}
	
	
	public void stopMocks() {
		LOG.info("Start");

		if (mocks == null) {
			return;
		}

		for (MockRunner исполнитель : listMocksRunner) {
			исполнитель.завершить_Работу();
		}

		for (Thread thread : threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				String mes = "Error while to wait mocks is stop of work" + e.getMessage();
				LOG.error(mes);
				System.out.println(mes);
			}
		}
		
		LOG.info("End");
	}


	public void setMocks(String fileConfigText) {
		
			String[] lines = fileConfigText.split("\n");
			boolean flagMock = false;
			String nameMock = "";
			int portMock = 8080;
			String fileResponse = "";
			for (String line : lines) {
				String lineTrim = line.trim();
				if (Config.isStartWith(lineTrim, "MOCK") && !flagMock) {
					if (mocks == null) {
						mocks = new ArrayList<>();
					}
					flagMock = true;
					nameMock = lineTrim.substring(5);
					continue;
				}
				if (flagMock) {
					if (Config.isStartWith(lineTrim, "PORT=")) {
						LOG.debug("PORT=" + lineTrim.substring(5));
						portMock = Integer.parseInt(lineTrim.substring(5));
					}

					if (Config.isStartWith(lineTrim, "RESPONSE=")) {
						fileResponse = lineTrim.substring(10, lineTrim.length() - 1);
					}
				}

				if (flagMock && lineTrim.isEmpty()) {
					flagMock = false;
					Mock mock = new Mock(nameMock, portMock, fileResponse);
					mocks.add(mock);
				}
			}
			LOG.debug("Mock=" + mocks);
		}


	public List<Mock> getMocks() {
		return mocks;
	}


	public List<Thread> getThreads() {
		return threads;
	}


	public List<MockRunner> getListMockRunner() {
		return listMocksRunner;
	}


}
