package ru.nsandrus.testing.dataobject;

import java.util.List;

public class TestSuiteNS {

		public Http http;
		public Jms jms;
		public ActivatorType activatorType;
		public String messagePatternSend;
		public String nameSuite;
		public String analizeLogFileName;
		public List<TestCase> testCase;
		public Server server;
		public List<Mocks> mocks;
		public boolean isDisabled;
		public int waitSecondsAnalizator;
		public String filterLogFile;

}
