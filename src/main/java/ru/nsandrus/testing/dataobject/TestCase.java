package ru.nsandrus.testing.dataobject;

import java.util.HashMap;
import java.util.List;

public class TestCase {

	public boolean isDisabled;
	public int clearWaitMocks;
	//public int waitlogsec;
	public long workedTime;
	public String nameCase;
	public String message;
	public Jms clearMqQueue;

	public List<Mocks> resetMocks;
	public HashMap<String, String> changeMessage;
	public List<HashMap<String, String>> changeMessageMocks;
	public AnalizatorItem analizatorData;
	// может лучше добавить AnalizatorLog сразу, но цепанет закрытые переменные тогда (не надо)
	public HashMap<String, ComponentStatus> componentGlassfish;

}
