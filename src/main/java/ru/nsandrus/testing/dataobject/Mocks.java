package ru.nsandrus.testing.dataobject;

public class Mocks {

	public int port;
	public String response;
	public String nameMock;
	public volatile String receive;

	public Mocks(Mocks заглушка) {
		this.port = заглушка.port;
		this.response = заглушка.response;
		this.nameMock = заглушка.nameMock;
		this.receive = заглушка.receive;
	}

	public Mocks() {
		this.port = 8080;
		this.response = "default response";
		this.nameMock = "Дефолтная заглушка";
		this.receive = "";

	}
}
