package ru.nsandrus.testing.dataobject;

public class Server {

	private String url;
	private String login;
	private String password;
	
	public Server(String url, String login, String password) {
		this.url = url;
		this.login = login;
		this.password = password;
	}
	
	public String getUrl() {
		return url;
	}

	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}

	
}
