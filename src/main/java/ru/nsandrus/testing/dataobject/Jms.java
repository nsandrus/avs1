package ru.nsandrus.testing.dataobject;

import java.util.Hashtable;

public class Jms {
	public int port;
	public String hostName;
	public String queueManagerName;
	public String channelName;
	public String queueName;
	public String userId;
	public String password;
	public String jmsMessage;
	public Hashtable<String, String> customProps;
	public int second;

}
