package ru.nsandrus.testing.dataobject;

public enum ComponentStatus {
	Start, Stop, Shutdown, SetBpelEngine, ResetBpelEngine;
}
