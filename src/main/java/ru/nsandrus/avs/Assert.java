package ru.nsandrus.avs;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.log4j.Logger;

import ru.nsandrus.file.FileReader;
import ru.nsandrus.file.ReadFileException;

public class Assert {
	enum Operation {
		PLUS, MINUS, ERROR, ANY, VARS
	};

	Logger LOG = Logger.getLogger(Assert.class);

	String data;
	private String filename;
	private String response = "";
	private String[] regs;
	private StringBuilder errorMessageFound = new StringBuilder();
	private String errorResponse = "";
	private static int TRIM_SIZE = 128;

	public Assert(String filename, String responseStep, HashMap<String, String> map) throws ReadFileException {
		if (responseStep == null) {
			logError("Constructor Assert, response is null");
			responseStep = "";
		}
		LOG.debug("filename=" + filename + "::response trim [" + TRIM_SIZE + "]=" + getTrimResponse(responseStep));
		this.filename = filename;
		String regexps = FileReader.readFile(filename);
		if (map != null) {
			for (Entry<String, String> pattern : map.entrySet()) {
				regexps = regexps.replaceAll("%" + pattern.getKey() + "%",
						Matcher.quoteReplacement(pattern.getValue()));
			}
		}
		LOG.debug("Loaded asserts::" + regexps);
		regs = regexps.split("\n");
		this.response = responseStep;
	}

	public boolean isPassedFilter() {
		LOG.info("filter assert is work");
		if (isEmptyData()) {
			return false;
		}
		return isPassedMatcher();
	}

	private boolean isPassedMatcher() {
		boolean pass = true;
		Operation operation;
		HashMap<String, String> vars = getVars(regs);
		if (!vars.isEmpty()) {
			regs = replaceVars(regs, vars);
		}

		for (String regexp : regs) {
			regexp = regexp.trim();
			LOG.trace("Check filter work::regexp=" + regexp);
			operation = getTypeOperation(regexp);
			if (operation.equals(Operation.ANY)) {
				continue;
			}
			regexp = regexp.substring(1);
			boolean isFindRegExp = false;
			String regexpWE = getEscapedString(regexp);
			try {
				Pattern p = Pattern.compile(regexpWE);
				Matcher m = p.matcher(response);
				isFindRegExp = m.find();
			} catch (PatternSyntaxException e2) {
				logError("\tERROR::syntax regexp new style::" + e2.getMessage() + "\t" + regexpWE);
				return false;
			}
			LOG.trace("is find " + isFindRegExp);
			if (!isFindRegExp) {
				LOG.trace("NOT FOUND ASSERT::" + response);
			}
			if (isFindRegExp && operation == Operation.MINUS) {
				errorMessageFound.append("Найдено ненужное=" + regexp + "\n");
				pass = false;
			} else if (!isFindRegExp && operation == Operation.PLUS) {
				errorMessageFound.append("Не найдено нужное=" + regexp + "\n");
				pass = false;
			} else if (operation == Operation.ERROR) {
				addErrorRegExp(regexp);
			}

		}
		if (!pass) {
			errorResponse = response;
			logError("\tFile regexp is |" + filename + "|");
		}
		return pass;
	}

	private String[] replaceVars(String[] regs2, HashMap<String, String> vars) {
		StringBuilder result = new StringBuilder();
		for (String line : regs2) {
			for (Entry<String, String> var : vars.entrySet()) {
				line = line.replace(var.getKey(), var.getValue());
			}
			result.append(line + "\n");
		}
		String allLine = result.toString();
		LOG.debug("new assert text=" + allLine);
		return allLine.split("\n");
	}

	private HashMap<String, String> getVars(String[] regs2) {
		LOG.debug("incoming message for search VARS is ::" + regs2);
		HashMap<String, String> result = new HashMap<>();
		for (String regline : regs2) {
			regline = regline.trim();
			LOG.trace("line of assert::" + regline);
			if (regline.startsWith(">")) {
				
				String nameVar = getVarName(regline);
				LOG.trace("found sibol '>'::VARS::NAME::" + nameVar);
				if (!nameVar.isEmpty()) {
					String nameValue = getVarValue(nameVar, regline);
					LOG.trace("found sibol '>'::VARS::VALUE::" + nameValue);
					if (!nameValue.isEmpty()) {
						result.put(nameVar, nameValue);
					}

				}
			}
		}
		LOG.debug("List of VARSS=" + result);
		return result;
	}

	private String getVarValue(String nameVar, String reglineIncoming) {
		String regline = reglineIncoming.substring(1);
		String newRegExp = regline.replace(nameVar, ".+");
		
		String value = "";
		
		
		try {
			String regexpWE = getEscapedString(newRegExp);
			LOG.trace("regexp for find vars VALUE::" + regexpWE);
			Pattern p2 = Pattern.compile(regexpWE);
			Matcher m2 = p2.matcher(response);
			boolean resErr = m2.find();
			LOG.trace("FOUND IS PATTERN IS " + resErr);
			if (resErr) {
				int start = m2.start();
				int end = m2.end();
				if (start == end) {
					LOG.trace("not found value of vars. start == end");
					return "";
				}
				String findWithValue = response.substring(start, end);
				LOG.trace("value of vars::" + findWithValue);
				int indexA = regline.indexOf(nameVar);
				int indexB = indexA + nameVar.length();
				String leftStr = regline.substring(0, indexA);
				String rightStr = regline.substring(indexB);

				String regexpLeft = getEscapedString(leftStr);
				Pattern p3 = Pattern.compile(regexpLeft);
				Matcher m3 = p3.matcher(regline);
				boolean isFind = m3.find();
				String ostatok = "";
				if (isFind) {
					int x = m3.start();
					int y = m3.end();
					if (x == y) {
						return "";
					}
					ostatok = findWithValue.substring(y);
				}

				String valueIs = "";
				String regexpRight = getEscapedString(rightStr);
				Pattern p4 = Pattern.compile(regexpRight);
				Matcher m4 = p4.matcher(ostatok);
				boolean isFind2 = m4.find();
				if (isFind2) {
					int x2 = m4.start();
					int y2 = m4.end();
					if (x2 == y2) {
						return "";
					}
					valueIs = ostatok.substring(0, x2);
				}
				return valueIs;
			}
		} catch (PatternSyntaxException e) {
			logError("Error in syntax regexp" + e.getMessage());
		}
		return value;
	}

	private String getVarName(String regline) {
		// в regLine надо удалить <%NAME_VAR%> и заменить на * или .+
		String result = "";
		try {
			Pattern p2 = Pattern.compile("<%.+%>");
			Matcher m2 = p2.matcher(regline);
			boolean resErr = m2.find();
			if (resErr) {
				int start = m2.start();
				int end = m2.end();
				result = regline.substring(start, end);
			}
		} catch (PatternSyntaxException e) {
			logError("Error in syntax regexp" + e.getMessage());
		}
		return result;
	}

	private String getEscapedString(String regexp) {
		String result = "";
		result = regexp.replace("\"", ("\\\""));
		result = result.replace("{", "\\{");
		result = result.replace("}", ("\\}"));
		result = result.replace("[", ("\\["));
		result = result.replace("]", ("\\]"));
		result = result.replace("(", ("\\("));
		result = result.replace(")", ("\\)"));
		result = result.replace("|", ("\\|"));
		result = result.replace("?", ("\\?"));
		LOG.debug("regexp with escape::" + result);
		return result;
	}

	private Operation getTypeOperation(String regexpLine) {
		Operation operation = Operation.ANY;
		if (regexpLine == null || regexpLine.isEmpty()) {
			return operation;
		}
		char simbol = regexpLine.charAt(0);
		if (simbol == '+') {
			operation = Operation.PLUS;
		} else if (simbol == '-') {
			operation = Operation.MINUS;
		} else if (simbol == '!') {
			operation = Operation.ERROR;
		} else if (simbol == '>') {
			operation = Operation.VARS;
		}
		LOG.trace("Operation is " + operation);
		return operation;
	}

	private boolean isEmptyData() {
		if (regs.length == 1 && regs[0].isEmpty()) {
			logError("RegExp is empty or not found file. See log for more details.");
			return true;
		}
		if (response == null) {
			String mes = "Analize assert, response is null";
			logError(mes);
			return true;
		}
		return false;
	}

	void addErrorRegExp(String regexp) {
		try {
			Pattern p2 = Pattern.compile(regexp);
			Matcher m2 = p2.matcher(response);
			boolean resErr = m2.find();
			if (resErr) {
				int start = m2.start();
				int end = m2.end();
				String str = response.substring(start, end);
				errorResponse = str;
			}
		} catch (PatternSyntaxException e) {
			logError("Error in syntax regexp" + e.getMessage());
		}
	}

	public String getErrorResponse() {
		return errorResponse;
	}

	public String getErrorMessage() {
		return errorMessageFound.toString();
	}

	protected void logError(String mes) {
		String mesTrim = getTrimResponse(mes);
		errorMessageFound.append(mesTrim);
		LOG.error(mes);
	}

	private String getTrimResponse(String mes) {
		int len = mes.length();
		if (len > TRIM_SIZE) {
			len = TRIM_SIZE;
			return mes.substring(0, len) + ".... see rest in the LOG file";
		}
		return mes;
	}

}
