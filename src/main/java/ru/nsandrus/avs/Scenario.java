package ru.nsandrus.avs;

import org.apache.log4j.Logger;

import ru.nsandrus.command.CommandMock;
import ru.nsandrus.command.CommandReadJms;
import ru.nsandrus.command.CommandSelect;
import ru.nsandrus.command.CommandWait;
import ru.nsandrus.entity.DomainGf;
import ru.nsandrus.exeptions.CommandException;
import ru.nsandrus.exeptions.ScenarioException;
import ru.nsandrus.file.ReadFileException;
import ru.nsandrus.glassfish.GlassFish;

import static ru.nsandrus.entity.KeyWords.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Scenario {

	Logger LOG = Logger.getLogger(Scenario.class);

	boolean isPassed;
	public String name;
	String path;
	public boolean isScenarioFor;
	int numForScenario;

	StringBuilder errorMessage = new StringBuilder();
	public StringBuilder code = new StringBuilder();

	ArrayList<HashMap<String, String>> replaceMap;

	public Scenario(ScenarioFor sc) {
		LOG.debug("scenario=" + sc.toString());
		isPassed = sc.isPassed;
		name = sc.name;
		path = sc.path;
		code = sc.code;
		isScenarioFor = true;
	}

	public Scenario() {
	}

	public void run(PlanAvs plan, HashMap<String, String> hashMap) throws ReadFileException, ScenarioException {
		path = plan.mainPath;
		plan.countScenario++;
		isPassed = false;
		LOG.info("Start scenario [" + plan.countScenario + "\t" + name.trim() + "]::\n" + this.toString());

		plan.managerServerLog.startWatchLogFile();

		System.out.printf("\n[%-5d %-40s]\n", plan.countScenario, name.trim());
		long startTime = System.currentTimeMillis();
		plan.report.addBeginScenario(startTime, plan.countScenario, name.trim());

		String responseStep = "";

		String[] lines = code.toString().split("\n");
		for (String line : lines) {
			if (line.trim().toUpperCase().startsWith(WAIT + " ")) {
				CommandWait.wait(line.trim());
			} else if (line.trim().toUpperCase().startsWith(COMMENT + " ") || line.trim().toUpperCase().startsWith(TESTPLAN)) {
				;
			} else if (line.trim().toUpperCase().startsWith(SET + " ")) {
				foundSet(plan, line);
			} else if (line.trim().toUpperCase().startsWith(ASSERT + " ")) {
				foundAssert(plan, hashMap, responseStep, line);
			} else if (line.trim().toUpperCase().startsWith(ASSERTLOG + " ")) {
				foundAssertLog(plan, hashMap, line);
			} else if (line.trim().toUpperCase().startsWith(ASSERTMOCK + " ")) {
				foundAssertMock(plan, hashMap, line);
			} else if (line.trim().toUpperCase().startsWith(ASSERTJMS + " ")) {
				foundAssertJms(plan, hashMap, line);
			} else if (line.trim().toUpperCase().startsWith(ASSERTSQL + " ")) {
				foundAssertSql(plan, hashMap, line);
			} else if (line.trim().toUpperCase().startsWith(ASSERTGF + " ")) {
				foundAssertGf(plan, hashMap, line);
			} else {
				responseStep = foundNothing(plan, hashMap, line);
			}
			sleep(10);
		}
		saveWorkTimeScenario(plan, startTime);
	}

	private static void sleep(int i) {
		try {
			Thread.sleep(i);
		} catch (InterruptedException e) {
		}
	}

	private void saveWorkTimeScenario(PlanAvs plan, long startTime) {
		long stopTime = System.currentTimeMillis();
		int workedTime = (int) ((stopTime - startTime) / 1000L);
		plan.report.addEndScenario(plan.countScenario, workedTime);
	}

	private String foundNothing(PlanAvs plan, HashMap<String, String> hashMap, String line) throws ReadFileException, ScenarioException {
		LOG.debug("run NoNameStep");
		Step step = new Step();
		step.name = "NoName";
		step.codeOfStep = line.trim();
		step.path = plan.mainPath;
		step.runnerMocks = plan.getListMocksRunner();
		step.threads = plan.getThreads();
		step.mocks = plan.getMocks();
		step.vars = plan.vars;
		String newResponseStep;
		try {
			newResponseStep = step.runCode(plan, hashMap);
		} catch (ScenarioException | CommandException e) {
			plan.report.addReporFailedScenario(plan.countScenario, e.getMessage());
			LOG.error("Exception in scenario::" + e.getMessage());
			throw new ScenarioException(e.getMessage());
		}
		return newResponseStep;
	}

	private void foundAssertSql(PlanAvs plan, HashMap<String, String> hashMap, String line) throws ScenarioException, ReadFileException {
		String filename = getFileName(line.substring(line.indexOf(" ") + 1));
		String filenameAssert = getSecondFileName(line.substring(line.indexOf(" ") + 1));
		String nameDb = getDbName(line);
		LOG.info("nameDb=" + nameDb + "::Filename=" + filename + "::");
		CommandSelect select = new CommandSelect(plan.databases, nameDb, plan.mainPath);
		String resultSelect = select.select(filename);
		Assert avsAssert = new Assert(filenameAssert, resultSelect, hashMap);
		String typeAssert = "ASSERTSQL";
		logResultOfPass(plan, avsAssert, typeAssert);
	}

	private void foundAssertJms(PlanAvs plan, HashMap<String, String> hashMap, String line) throws ScenarioException, ReadFileException {
		String filename = getFileName(line.substring(line.indexOf(" ") + 1));
		String nameQueue = getQueueName(line);
		CommandReadJms readJms = new CommandReadJms(plan.queues);
		String mqJson = readJms.getAllMessages(nameQueue);
		LOG.debug("AsserJms::filename=" + filename);
		Assert avsAssert = new Assert(filename, mqJson, hashMap);
		String typeAssert = "ASSERTJMS";
		logResultOfPass(plan, avsAssert, typeAssert);
	}

	private void foundAssertMock(PlanAvs plan, HashMap<String, String> hashMap, String line) throws ScenarioException, ReadFileException {
		String receiveMock;
		String filename = getFileName(line);
		CommandMock comMock = new CommandMock();
		receiveMock = comMock.getReceiveText(plan.getMocks());
		LOG.debug("AsserMock::filename=" + filename);
		Assert avsAssert = new Assert(filename, receiveMock, hashMap);
		String typeAssert = "ASSERTMOCK";
		logResultOfPass(plan, avsAssert, typeAssert);
	}

	private void foundAssertLog(PlanAvs plan, HashMap<String, String> hashMap, String line) throws ScenarioException, ReadFileException {
		plan.managerServerLog.pause();
		String typeAssert = "ASSERTLOG";
		if (plan.managerServerLog.isEmpty()) {
			plan.report.addReporFailed(plan.countScenario, typeAssert, "NOT FOUND Server.log", "");
			throw new ScenarioException("Filename for server.log is empty!");
		}

		String filename = getFileName(line);
		LOG.debug("AssertLog::filename=" + filename);
		Assert avsAssert = new Assert(filename, plan.managerServerLog.getData(), hashMap);
		logResultOfPass(plan, avsAssert, typeAssert);
	}

	private void foundAssert(PlanAvs plan, HashMap<String, String> hashMap, final String responseStep, String line) throws ScenarioException, ReadFileException {
		String filename = getFileName(line);
		LOG.debug("Assert::filename=" + filename);
		Assert avsAssert = new Assert(filename, responseStep, hashMap);
		String typeAssert = "ASSERT";
		logResultOfPass(plan, avsAssert, typeAssert);
	}

	private void foundAssertGf(PlanAvs plan, HashMap<String, String> hashMap, String line) throws ScenarioException, ReadFileException {
		String filename = getFileName(line);
		LOG.debug("AssertGf::filename=" + filename);
		LOG.debug("AssertGf::line=" + line);
		String nameDomain = getDomainName(line);
		LOG.debug("AssertGf::domain=" + nameDomain);
		String commandName = getCommandName(line);
		LOG.debug("AssertGf::command=" + commandName);
		DomainGf domain = getDomain(plan.domainsGf, nameDomain);
		LOG.debug("found domain " + domain);
		GlassFish server = new GlassFish(domain);

		if (commandName.equalsIgnoreCase("BPEL")) {
			String bpel = server.getBpelEngine();
			Assert avsAssert = new Assert(filename, bpel, hashMap);
			String typeAssert = "ASSERTGF BPEL";
			logResultOfPass(plan, avsAssert, typeAssert);
		}
	}

	private DomainGf getDomain(List<DomainGf> domainsGf, String nameDomain) throws ScenarioException {
		for (DomainGf domain : domainsGf) {
			if (domain.name.equalsIgnoreCase(nameDomain)) {
				return domain;
			}
		}
		throw new ScenarioException("Not found domain flassfish " + nameDomain);
	}

	private void logResultOfPass(PlanAvs plan, Assert avsAssert, String typeAssert) {
		LOG.info("result of pass " + typeAssert);
		boolean passAssert = avsAssert.isPassedFilter();
		if (!passAssert) {
			plan.countFailed++;
			plan.report.addReporFailed(plan.countScenario, typeAssert, avsAssert.getErrorMessage(), avsAssert.getErrorResponse());
		} else {
			isPassed = true;
			plan.countPassed++;
			plan.report.addReportPassed(plan.countScenario, typeAssert);
		}
	}

	private void foundSet(PlanAvs plan, String line) throws ScenarioException {
		String[] secondCommand = line.trim().toUpperCase().split(" ");
		StringBuilder sets = new StringBuilder();
		for (String parm : secondCommand) {
			sets.append(parm + "::");
		}
		LOG.debug("SET is consist::" + sets.toString() + "::length=" + secondCommand.length);
		if (secondCommand.length < 2) {
			String mes = "Not found second command of SET";
			throw new ScenarioException(mes);
		}

		if (secondCommand[1].equals(HTTP_TIMEOUT)) {
			if (secondCommand[2].toUpperCase().startsWith("DEF")) {
				plan.HttpTimeOut = plan.HttpTimeOutDefault;
			} else {
				int newTimeOut = Integer.parseInt(secondCommand[2]);
				plan.HttpTimeOut = newTimeOut;
				System.out.println("SET HttpTimeOut is " + newTimeOut);
			}
		}
	}

	private String getQueueName(String line) throws ScenarioException {
		String[] parts = line.split(" ");
		if (parts == null || parts.length < 2) {
			throw new ScenarioException("Nof found name for read Queue JMS Message");
		}
		String name = parts[1];
		return name;
	}

	private String getDbName(String line) throws ScenarioException {
		String[] parts = line.split(" ");
		if (parts == null || parts.length < 2) {
			throw new ScenarioException("Nof found name DB");
		}
		String name = parts[1];
		return name;
	}

	private String getDomainName(String line) throws ScenarioException {
		String[] parts = line.split(" ");
		if (parts == null || parts.length < 3) {
			throw new ScenarioException("Nof found name Domain");
		}
		String name = parts[2];
		return name;
	}

	private String getCommandName(String line) throws ScenarioException {
		String[] parts = line.split(" ");
		if (parts == null || parts.length < 2) {
			throw new ScenarioException("Nof found command name for AsserGF");
		}
		String name = parts[1];
		return name;
	}

	private String getFileName(String line) throws ScenarioException {
		String[] parts = line.trim().split("\"");
		if (parts.length < 2) {
			String mes = "Not found argument filename for assert";
			// System.out.println(mes);
			LOG.error(mes);
			throw new ScenarioException(mes);
		}
		return path + parts[1];
	}

	private String getSecondFileName(String line) throws ScenarioException {
		String[] parts = line.trim().split("\"");
		if (parts.length < 4) {
			String mes = "Not found argument filename for assert";
			// System.out.println(mes);
			LOG.error(mes);
			throw new ScenarioException(mes);
		}
		return path + parts[3];
	}

	@Override
	public String toString() {
		return "Scenario name=" + name + ", \npath=" + path + ", \ncode=" + code + "]";
	}

}
