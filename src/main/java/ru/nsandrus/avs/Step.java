package ru.nsandrus.avs;

import static ru.nsandrus.entity.KeyWords.*;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import ru.nsandrus.command.CommandMock;
import ru.nsandrus.command.CommandSendJms;
import ru.nsandrus.command.CommandSendSoap;
import ru.nsandrus.entity.MQueueEntity;
import ru.nsandrus.entity.ResultEntitySoap;
import ru.nsandrus.exeptions.CommandException;
import ru.nsandrus.exeptions.ScenarioException;
import ru.nsandrus.file.ReadFileException;
import ru.nsandrus.mock.Mock;
import ru.nsandrus.mock.MockRunner;

public class Step {

	private static final String NO_SECOND_COMMAND = "NO";

	static Logger LOG = Logger.getLogger(Step.class);

	public List<Mock> mocks;
	public List<MockRunner> runnerMocks;
	public List<Thread> threads;
	public List<MQueueEntity> mqueues;
	public HashMap<String, String> vars;

	private int HttpTimeOut;
	String codeOfStep;
	String name;
	String path = "";

	private String responseSoap;

	StringBuilder errorMessage = new StringBuilder();
	ResultEntitySoap result;

	private HashMap<String, String> hashMap;

	public String runCode(PlanAvs plan, HashMap<String, String> hashMap) throws ScenarioException, ReadFileException, CommandException {
		LOG.trace("vars=" + vars);
		this.hashMap  = hashMap;
		HttpTimeOut = plan.HttpTimeOut;
		mqueues = plan.queues;
			runPassedCode();
			return responseSoap;
	}

	private void runPassedCode() throws ScenarioException, ReadFileException, CommandException {
		String firstCommand = getFirstCommand(codeOfStep);
		LOG.info("First command is " + firstCommand);

		if (firstCommand.equals(SENDSOAP)) {
			commandSendSoap();
		} else if (firstCommand.equals(SENDJMS)) {
			commandSendJms();
		} else if (firstCommand.equals(MOCK)) {
			commandMock();
		} else {
			LOG.error("not found command " + firstCommand);
			throw new CommandException("not found command " + firstCommand + "::");
		}
	}

	private void commandMock() throws ReadFileException, ScenarioException, CommandException {
		CommandMock comMock = new CommandMock();
		String command = getSecondCommand(codeOfStep).trim();
		LOG.debug("second command of MOCK=" + command);
		if (command.equals(STOP)) {
			comMock.stop(mocks, codeOfStep, runnerMocks);
		} else if (command.equals(START)) {
			comMock.start(mocks, codeOfStep, runnerMocks, path, threads);
		} else if (command.equals(NEWRESPONSE)) {
			comMock.newResponse(mocks, codeOfStep, path);
		} else if (command.equals(REPLACE)) {
			comMock.replaceResponse(mocks, codeOfStep, path, vars);
		} else if (command.equals(REPEAT)) {
			comMock.repeateResponse(mocks, codeOfStep, path, vars, hashMap);
		} else if (command.equals(NO_SECOND_COMMAND)) {
			System.out.println("SKIP ANY COMMAND" + command);
		} else {
			throw new CommandException("No valid second command is [" + command + "]\t");
		}
	}

	private void commandSendJms() throws ReadFileException, ScenarioException {
		CommandSendJms sendJms = new CommandSendJms(path, codeOfStep, mqueues);
		result = sendJms.exec(hashMap);
		if (!result.isOk) {
			logError("STEP[" + name + "] ERROR " + result.error);
		}
	}

	private void commandSendSoap() throws ScenarioException  {
		CommandSendSoap sendSoap = new CommandSendSoap(path, codeOfStep, HttpTimeOut, vars);
		try {
			result = sendSoap.getResultSoapRequest(hashMap);
		} catch (ScenarioException e) {
			throw new ScenarioException("Error send Soap message::" + e.getMessage());
		}
		if (!result.isOk) {
			logError("STEP[" + name + "] ERROR " + result.error);
		}
		responseSoap = result.result;
	}

	public static String getFirstCommand(String codeOfStep) throws CommandException {
		int end = codeOfStep.indexOf(" ");
		if (end < 0) {
			throw new CommandException("Not found first command of scenaion!");
		}
		String command = codeOfStep.substring(0, end);
		return command.toUpperCase();
	}

	private String getSecondCommand(String codeLine) {
		String[] commands = codeLine.split(" ");
		if (commands.length < 2) {
			return NO_SECOND_COMMAND;
		}
		return commands[1].toUpperCase();
	}

	@Override
	public String toString() {
		return "\t" + name + " : " + codeOfStep + "";
	}

	protected void logError(String mes) {
//		System.out.println(mes);
		LOG.error(mes);
		errorMessage.append(mes);
	}

}
