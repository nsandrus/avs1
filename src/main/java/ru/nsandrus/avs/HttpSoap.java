package ru.nsandrus.avs;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;

import ru.nsandrus.entity.SoapMessage;
import ru.nsandrus.exeptions.ScenarioException;

public class HttpSoap {

	Logger LOG = Logger.getLogger(HttpSoap.class);

	private int timeout;
	private String urlstr;
	private String parameters;
	private String username;
	private String password;
	private String defaultContentType = "text/xml;charset=UTF-8";

	private String contentType;

	public String postDataAuthorize(SoapMessage soapMessage, int timeOut) throws ScenarioException {
		this.urlstr = soapMessage.url;
		this.parameters = soapMessage.message;
		this.username = soapMessage.login;
		this.password = soapMessage.pass;
		this.timeout = timeOut;
		this.contentType = soapMessage.contentType;
		if (this.contentType == null){
			return postDataAuthorize(defaultContentType);
		} else {
			return postDataAuthorize(this.contentType);
		}
	}

	private String postDataAuthorize(String contentType) throws ScenarioException {

		if (username == null)
			username = "";
		if (password == null)
			password = "";
		URL url;
		try {
			url = new URL(urlstr);
		} catch (MalformedURLException e1) {
			throw new ScenarioException("Error get new URL::" + e1.getMessage());
		}
		LOG.debug("START::URL=" + urlstr + "::login=" + username + "::pass=" + password);
		LOG.trace("START::parametres=" + parameters);
		HttpURLConnection hpcon = null;

		try {
			hpcon = sendRequest(parameters, username, password, contentType, timeout, url);
		} catch (IOException e) {
			throw new ScenarioException("Exception of write to HTTP::" + e.getMessage());
		}

		int codeResponse = 0;
		try {
			codeResponse = hpcon.getResponseCode();
		} catch (IOException e1) {
			throw new ScenarioException("Error connection to " + urlstr + "::" + e1.getMessage());
		}

		BufferedReader in = null;
		LOG.info("code response=" + codeResponse);
		if (codeResponse >= 500) {
			try {
				in = new BufferedReader(new InputStreamReader(hpcon.getErrorStream(), "UTF-8"));
			} catch (IOException e) {
				throw new ScenarioException("exeption read error stream ");
			}
		} else {
			try {
				in = new BufferedReader(new InputStreamReader(hpcon.getInputStream(), "UTF-8"));
			} catch (IOException e) {
				LOG.debug("exeption read good stream " + e.getMessage());
				throw new ScenarioException("exeption read good stream " + e.getMessage());
			}
		}

		StringBuilder response = getResponse(hpcon, in);
		return response.toString();
	}

	private StringBuilder getResponse(HttpURLConnection hpcon, BufferedReader in) throws ScenarioException {
		String input;
		StringBuilder response = new StringBuilder();
		try {
			while ((input = in.readLine()) != null) {
				response.append(input + "\r");
			}
		} catch (IOException e) {
			throw new ScenarioException("Ошибка чтения потока HTTP." + e.getMessage());
		}
		if (hpcon != null) {
			hpcon.disconnect();
		}
		return response;
	}

	private HttpURLConnection sendRequest(String parameters, String username, String password, String contentType,
			int timeOut, URL url) throws IOException, ProtocolException, UnsupportedEncodingException {
		HttpURLConnection hpcon;
		String userPassword = username + ":" + password;
		String encoding = DatatypeConverter.printBase64Binary(userPassword.getBytes());
		hpcon = (HttpURLConnection) url.openConnection();
		hpcon.setRequestMethod("POST");
		hpcon.setRequestProperty("Content-Length", "" + Integer.toString(parameters.getBytes().length));
		hpcon.setRequestProperty("Authorization", "Basic " + encoding);
		// hpcon.setRequestProperty("Content-Language", "en-US");
		hpcon.setRequestProperty("Content-Type", contentType);
		hpcon.setRequestProperty("Connection", "Keep-Alive");
		hpcon.setRequestProperty("SOAPAction", "");
		hpcon.setUseCaches(false);
		// getting the response is required to force the request, otherwise it
		// might not even be sent at all
		hpcon.setReadTimeout(timeOut * 1000);
		hpcon.setConnectTimeout(timeOut * 1000);
		hpcon.setDoInput(true);
		hpcon.setDoOutput(true);
		DataOutputStream printout = new DataOutputStream(hpcon.getOutputStream());
		LOG.debug("SEND::Data=" + parameters);
		printout.write(parameters.getBytes("UTF-8"));
		printout.flush();
		printout.close();
		return hpcon;
	}

	protected void sleep(int sec) {
		try {
			Thread.sleep(sec * 1000);
		} catch (InterruptedException e) {
			;
		}
	}
}
