package ru.nsandrus.avs;

import org.apache.log4j.Logger;

import ru.nsandrus.file.FileReader;
import ru.nsandrus.file.ReadFileException;

public class TestAvs1 {

	static Logger LOG = Logger.getLogger(TestAvs1.class);

	static final String VERSION = "0.9.5.1";
	static String codeAvsAll;

	public static void main(String[] args) {
		System.out.println("Start AVS version " + VERSION);
		try {
			loadTestPlan(args);
			PlanAvs plan = PlanAvs.getPlan(codeAvsAll);
			plan.start();
			plan.printReport();
			plan.saveReport();
		} catch (ReadFileException e) {
			System.out.println("EXCEPTION occured");
		}
		System.out.println("\nPlan is finish!");
		pressAnyKeyToExit();
		System.exit(0);
	}

	private static void loadTestPlan(String[] args) throws ReadFileException {
		if (args.length < 1) {
			throw new ReadFileException("Error, no file avs in argument");
		}
		readFiles(args);
	}

	private static void readFiles(String[] args) throws ReadFileException {
		System.out.println("Search Plan file [" + args[0] + "]");
		codeAvsAll = FileReader.readFile(args[0]);
	}

	private static void pressAnyKeyToExit() {
		System.out.println("Press <Enter> key to exit.");
		try {
			System.in.read();
		} catch (Exception e) {
		}
	}
}
