package ru.nsandrus.avs;

import static ru.nsandrus.entity.KeyWords.HTTP_TIMEOUT;
import static ru.nsandrus.entity.KeyWords.TESTPLAN;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;

import ru.nsandrus.analizatorLog.ManagerServerLog;
import ru.nsandrus.entity.DbEntity;
import ru.nsandrus.entity.DomainGf;
import ru.nsandrus.entity.MQueueEntity;
import ru.nsandrus.enviroment.Config;
import ru.nsandrus.exeptions.ScenarioException;
import ru.nsandrus.file.ReadFileException;
import ru.nsandrus.mock.ManagerMock;
import ru.nsandrus.mock.Mock;
import ru.nsandrus.mock.MockRunner;
import ru.nsandrus.report.Report;

public class PlanAvs {

	Logger LOG = Logger.getLogger(PlanAvs.class);
	
	private static final String SERVER_LOG = "SERVERLOG=";

	public int workedTime;
	public int HttpTimeOut;
	public int HttpTimeOutDefault;
	public int countScenario;
	public int countPassed;
	public int countFailed;

	public String mainPath;
	public String name;

	String fileConfigText = "";

	List<Scenario> scenaries;
	List<MQueueEntity> queues;
	List<DbEntity> databases;
	List<DomainGf> domainsGf;
	
	HashMap<String, String> vars;
	Report report = new Report();
	ManagerMock managerMock = new ManagerMock();
	ManagerServerLog managerServerLog = new ManagerServerLog();
	

	public String codeAvs;

	public static PlanAvs getPlan(String codeAvsAll) throws ReadFileException {
		PlanAvs plan = new PlanAvs();
		plan.fileConfigText = getSectionCfg(codeAvsAll);
		plan.codeAvs = getSectionCode(codeAvsAll);
		plan.queues = Config.getQueues(plan.fileConfigText);
		plan.setServerLog();
		plan.databases = Config.getDataBases(plan.fileConfigText);
		plan.vars = Config.getVars(plan.fileConfigText);
		plan.mainPath = Config.getMainPath(plan.fileConfigText);
		plan.scenaries = Config.getScenaries(plan.codeAvs);
		plan.name = Config.getPlanName(plan.codeAvs);
		plan.setHttpTimeOut();
		plan.HttpTimeOutDefault = plan.HttpTimeOut;
		plan.domainsGf = Config.getDomainsGf(plan.fileConfigText);
		return plan;
	}

	
	private static String getSectionCode(String codeAvsAll) {
		int index = codeAvsAll.indexOf(TESTPLAN);
		String resultCode = codeAvsAll.substring(index);
		return resultCode;
	}

	
	private static String getSectionCfg(String codeAvsAll) {
		int index = codeAvsAll.indexOf(TESTPLAN);
		String resultCfg = codeAvsAll.substring(0,index); 
		return resultCfg;
	}
	
	


	public void start() throws ReadFileException {
		LOG.debug("Start:Plan name=" + name);
		long startTime = System.currentTimeMillis();
		
		managerMock.setMocks(fileConfigText);
		managerMock.startMocks(mainPath);
		managerServerLog.startServerLog();

		runScenaries();

		managerMock.stopMocks();
		managerServerLog.stopServerLog();
		sleep(1);
		long stopTime = System.currentTimeMillis();
		workedTime = (int) ((stopTime - startTime) / 1000L);
		
	}

	private static void sleep(int i) {
		try {
			Thread.sleep(i * 1000);
		} catch (InterruptedException e) {
			;
		}
	}
	
	private void runScenaries() {
		for (Scenario scenario : scenaries) {
			try {
				if (scenario.isScenarioFor) {
					runScenarioFor(scenario, this);
				} else {
					scenario.run(this, null);
				}
			} catch (ReadFileException | ScenarioException e) {
				LOG.error("scenarion = " + countScenario + "::" +e.getMessage());
				countFailed++;
				report.addReportFatal(e.getMessage(), countScenario);
				continue;
			}
			
		}
	}

	private void runScenarioFor(Scenario sc, PlanAvs planAvs) throws ReadFileException, ScenarioException {
		LOG.debug("found FOR scenarion");
		ScenarioFor scFor = new ScenarioFor(sc);
		scFor.run(this);
	}
	
	
	public void setHttpTimeOut() {
		int timeOut = 5;
		String[] blocks = fileConfigText.split("\r\n\r\n");
		for (String lineCfg : blocks) {
			if (lineCfg.trim().toUpperCase().startsWith(HTTP_TIMEOUT+"=")) {
				timeOut = Integer.parseInt(lineCfg.trim().substring(12));
			}
		}
		LOG.debug("HttpTimeOut set is " + timeOut);
		HttpTimeOut = timeOut;
	}
	

	public void setServerLog() {
		String result = "";

		String[] lines = fileConfigText.split("\r\n");
		for (String line : lines) {
			if (line.trim().toUpperCase().startsWith(SERVER_LOG)) {
				result = line.trim().substring(SERVER_LOG.length());
			}
		}
		LOG.info("ServerLog=[" + result + "]");
		managerServerLog.setServerLog(result);
	}
	

	public void printReport() {
		report.вывести_Отчет(this);
	}

	
	public void saveReport() {
		try {
			report.вывести_Отчет_Файл(this);
			report.вывести_Отчет_Файл_HTML(this);
		} catch (UnsupportedEncodingException e) {
			System.out.println("Ошибка при создании отчета в файл." + e.getMessage());
		}
	}

	


	@Override
	public String toString() {
		return "PlanAvs " + name + "\nSCENARIES=" + scenaries;
	}


	public List<Mock> getMocks() {
		return managerMock.getMocks();
	}


	public List<Thread> getThreads() {
		return managerMock.getThreads(); 
	}


	public List<MockRunner> getListMocksRunner() {
		return managerMock.getListMockRunner();
	}

}
