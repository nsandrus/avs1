package ru.nsandrus.avs;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;
import com.ibm.mq.jms.MQQueueConnectionFactory;
import com.ibm.msg.client.wmq.WMQConstants;

import ru.nsandrus.entity.MQueueEntity;
import ru.nsandrus.exeptions.ScenarioException;

import java.util.Enumeration;
import java.util.Hashtable;


public class MQJson {

	final static Logger LOG = Logger.getLogger(MQJson.class);

//	private static final String CLASS = "CLASS JMS";
	
	private int port;
	private String hostName;
	private String queueManagerName;
	private String channelName;
	private String queueName;
	private String userId;
	private String password;
	private String message;
	private Hashtable<String, String> customProps;

//	public static void main(String[] args) {
//	
//		  //queueNameList=NXP, message={"operation":"addSOC","edoid": "20725363452", "ban":"999016144","FTRCD_EDIACC":"EDIACC","FTRCD_EDIACC_SOCNAME":"EDO10","FTRCD_EDIACC_NAME":"AVSurkov","FTRCD_EDIACC_SERVER":"pnz.beeline.ru","FTRCD_EDIACC_NUMBER":"1"}, props={}]|#]
//		MQueueEntity queue = new MQueueEntity();
//		queue.channel = "TEST_2.CHL";
//		queue.queueManager = "QM.FOR.TEST";
//				queue.hostName = "dr-glass016";
//				queue.port = 1450;
//				queue.password = "*";
//				queue.user = "*";
//				queue.queueName ="atc.beepayxp";
//				queue.message = "{\"operation\":\"addSOC\"}";
//		MQJson mqjson = new MQJson(queue);
//		mqjson.SendMessage();
//		System.out.println("all ok");
//	}
	
	
	public MQJson(MQueueEntity queue) {
		this.port = queue.port;
		this.hostName = queue.hostName;
		this.queueManagerName = queue.queueManager;
		this.channelName = queue.channel;
		this.queueName = queue.queueName;
		this.userId = queue.user;
		this.password = queue.password;
		this.customProps = queue.props;
		this.message = queue.message;
	}

	public void SendMessage() throws ScenarioException {
		LOG.debug("send message to MQ::Data host=" + hostName + ":" + port + "::queue=" + queueName);
		MQQueueConnectionFactory queueConnectionFactory = new MQQueueConnectionFactory();
		LOG.debug("factory ok");
		queueConnectionFactory.setHostName(hostName);
		try {
			queueConnectionFactory.setPort(port);
			queueConnectionFactory.setTransportType(WMQConstants.WMQ_CM_CLIENT);
			queueConnectionFactory.setQueueManager(queueManagerName);
			queueConnectionFactory.setChannel(channelName);
		} catch (JMSException e) {
			throw new ScenarioException("!!!Ошибка при подключении к MQ:" + e.getMessage());
		}
		try {

			Connection connection = queueConnectionFactory.createConnection(userId, password);
			Session session = connection.createSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
			Queue queue = session.createQueue("queue:///" + this.queueName);
			Message message = session.createTextMessage(this.message);
			MessageProducer messageProducer = session.createProducer(queue);

			Enumeration<String> en = customProps.keys();
			while (en.hasMoreElements()) {
				String key = (String) en.nextElement();
				String value = (String) customProps.get(key);
				message.setStringProperty(key, value);
			}
			messageProducer.send(message);
			messageProducer.close();
			session.close();
			connection.close();
		}

		catch (JMSException jmse) {
			throw new ScenarioException("Error to put JMS message::" + jmse.getMessage());
		}
	}

	public String ReadMessage() throws ScenarioException {
		
		LOG.debug("read message from MQ::Data host=" + hostName + ":" + port + "::queue=" + queueName);
		MQQueueConnectionFactory queueConnectionFactory = new MQQueueConnectionFactory();
		LOG.debug("factory ok");
		queueConnectionFactory.setHostName(hostName);
		try {
			queueConnectionFactory.setPort(port);
			queueConnectionFactory.setTransportType(WMQConstants.WMQ_CM_CLIENT);
			queueConnectionFactory.setQueueManager(queueManagerName);
			queueConnectionFactory.setChannel(channelName);
		} catch (JMSException e) {
			throw new ScenarioException("!!!Ошибка при подключении к MQ:" + e.getMessage());
		}
		try {

			Connection connection = queueConnectionFactory.createConnection(userId, password);
			Session session = connection.createSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
			Queue queue = session.createQueue("queue:///" + this.queueName);
			MessageConsumer messageConsumer = session.createConsumer(queue);
			connection.start();
			
			StringBuffer allMessages = new StringBuffer();
			Message message;
			boolean end = false;
			while (!end)
			{
			    message = messageConsumer.receiveNoWait();
			    if (message == null) {
			    	break;
			    }
			    if (message instanceof TextMessage)
			    {
			        allMessages.append(((TextMessage) message).getText());
			    }
			    else
			    {
			        byte[] body = new byte[(int) ((BytesMessage) message).getBodyLength()];
			        ((BytesMessage) message).readBytes(body);
			        allMessages.append(new String(body));
			    }
			        LOG.info(": Received  message:  " + allMessages.toString());
			}
			
			messageConsumer.close();
			session.close();
			connection.close();
			return allMessages.toString();
		}

		catch (JMSException jmse) {
			throw new ScenarioException("Error while read JMS message::" + jmse.getMessage());
		}
	}
}