package ru.nsandrus.avs;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.regex.Matcher;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;

import ru.nsandrus.command.CommandReplace;
import ru.nsandrus.exeptions.ScenarioException;
import ru.nsandrus.file.ReadFileException;

public class ScenarioFor extends Scenario{

	Logger LOG = Logger.getLogger(ScenarioFor.class);
	
	StringBuilder origCode;
	List<String> origAsserts;
	
	public ScenarioFor(Scenario sc) {
		LOG.debug("scenario=" + sc.toString());
		isPassed = sc.isPassed;
		name = sc.name;
		path = sc.path;
		code = sc.code;
		origCode = new StringBuilder(sc.code.toString());	
		errorMessage = sc.errorMessage;
	}

	public void run(PlanAvs plan) throws ReadFileException, ScenarioException {
		LOG.debug("Start scenarioFor");
		path = plan.mainPath;
		isPassed = false;
		LOG.debug("code scenario=" + code.toString());

		if (code.length() == 0) {
			System.out.println("Strange, but code of ScenarioFor is empty. Len=" + code.length());
			return;
		}

		String mainPath = removeQuotas(name);  
		
		super.replaceMap = getReplaceMap(mainPath);
		if (replaceMap == null) {
			System.out.println("LIST is NULL, sorry. Stop scenario.");
			return;
		}
		LOG.info("LIST MAIN FOR=" + replaceMap.toString());
		numForScenario = 0;
		String prefixName = getNameScenarioFor(name);
		
		for ( HashMap<String, String> lineMap :replaceMap) {
			StringBuilder nameScenario = new StringBuilder(prefixName);
			StringBuilder newcode = new StringBuilder(origCode.toString());
			List<String> newAssert = new ArrayList<>();
			if (origAsserts != null) {
				newAssert = new ArrayList<>(origAsserts);
			}
			for ( Entry<String, String> k : lineMap.entrySet()) {
				nameScenario.append(k.getKey() + "=" + k.getValue() + ";"); 
				newcode = replaceVars(newcode,k.getKey(),k.getValue());
				newAssert = replaceAsserts(newAssert,k.getKey(),k.getValue());
			}
			LOG.info("replace to normal new scenario::" + newcode.toString());
			name = nameScenario.toString();
			code = newcode;
			super.run(plan, replaceMap.get(numForScenario));
			numForScenario++;
		}
	}

	private String getNameScenarioFor(String mainPath) {
		int indexEnd = mainPath.indexOf("\"", 1);
		return mainPath.substring(indexEnd+1).trim() + "::";
	}
	
	
	private List<String> replaceAsserts(List<String> newAssert, String key, String value) {
		List<String> response = new ArrayList<>();
		for (String newList : newAssert) {
			response.add(newList.replaceAll("\\$\\{" + key + "\\}", Matcher.quoteReplacement(value)));
		}
		return response;
	}

	private StringBuilder replaceVars(StringBuilder code2, String key, String value) {
		StringBuilder response = new StringBuilder();
		response.append(code2.toString().replaceAll("\\$\\{" + key + "\\}", Matcher.quoteReplacement(value)));
		return response;
	}

	private String removeQuotas(String mainPath) {
		int indexEnd = mainPath.indexOf("\"", 1);
		return mainPath.substring(1, indexEnd).trim();
	}

	private ArrayList<HashMap<String, String>> getReplaceMap(String filename) throws ScenarioException {

		File file = new File(path + filename);
		String data = "";
		try {
			data = FileUtils.readFileToString(file, "UTF-8");
		} catch (IOException e) {
			String textError = "Error read message from file " + file.getAbsolutePath() + "\n" + e.getMessage();
			throw new ScenarioException(textError);
		}
		ArrayList<HashMap<String, String>> replaceMap = null;
		try {
			replaceMap= CommandReplace.jsonString2Array(data);
		} catch (JSONException e) {
			throw new ScenarioException("Error format of JSON file." + e.getMessage());
		}

		return replaceMap;
	}
	
}
